<!-- start: PAGE TITLE -->
<style>
.rojo {
    color: #ff6666;
}
.amarillo {
    color: #cccc00;
}
.azul {
    color: #4db8ff;
}
.verde {
    color: #00cc00;
}
.azul2 {
    color: #0000ff;
}
.morado {
    color: #5900b3;
}
.crema {
    color: #ff66ff;
}
.gris {
    color: #d1d1e0;
}
.negro {
    color: #000000;
}
.oro {
    color: #808000;
}
.naranja {
    color: #ff9933;
}
.selected {
    font-weight:bold;
    background-color: #ccffcc;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="mainTitle">Calendario de Contactos y Citas</h1>
        </div>
    </div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesCiviles -->
<section ng-controller="calendario" ng-init="init()">
    <div class="row margin-top-15 margin-bottom-15">
        <div class="col-sm-12 col-md-6">
            <div class="row">
                <div class="col-sm-12 col-md-3 text-right">
                    <label for="filtrocalendario.tipoevento">
                        Evento
                    </label>
                </div>
                <div class="col-sm-12 col-md-9">
                    <select class="form-control" ng-model='filtrocalendario.tipoevento' ng-change="rerenderCalendar()">
                        <option value='Ambos'>Ambos</option>
                        <option value='Contacto'>Contacto</option>
                        <option value='Cita'>Cita</option>
                    </select>
                </div>
            </div> 
        </div>
        <div class="col-sm-12 col-md-6" ng-if="tipocalendario=='administrador'">
            <div class="row">
                <div class="col-sm-12 col-md-3 text-right">
                    <label for="filtrocalendario.ejecutivo">
                        Ejecutivo
                    </label>
                </div>
                <div class="col-sm-12 col-md-9">
                    <select class="form-control" ng-model='filtrocalendario.ejecutivo' ng-change="rerenderCalendar()">
                        <option ng-repeat="ejecutivo in usuarioscalendario" value={{ejecutivo}}>{{ejecutivo}}</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div ui-calendar="uiConfig.calendar" calendar="myCalendar1" ng-model="agenda_calendario.events"></div>
</section>
<!-- end: CONSULTA DE ExpedientesCiviles -->
