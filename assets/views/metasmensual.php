<!-- start: PAGE TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-12">
			<h1 class="mainTitle">Tablero de Control (Metas Mensuales)</h1>
		</div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesCiviles -->
<section ng-controller="metasmensual" ng-init="init_metas2()">
   <div class="row">

      <div class="col-md-3">
         <div class="panel panel-white {{wait}}">
            <div class="panel-heading border-light bg-orange">
               <h4 class="panel-title"><b>LLAMADAS DIARIAS</b></h4>
            </div>
            <div class="panel-body">
               <div class="height-200 animate-show-hide">
                  <table ng-table="tableParamsMetas_di" show-filter="false" class="table table-condensed table-hover">
                     <tr>
                         <th></th>
                         <th><center>EJECUTIVO</th>
                         <th><center>LLAMADAS</th>
                         <th><center>CONTACTOS EFECTIVOS</th>
                     </tr>
                     <tr ng-repeat="Metas_ll in $data" >
                        <td width="33%" ng-if="Metas_ll.shortname == 'Ceci'">  <center><img src="assets/images/Cecilia.jpg" width="30%"></td>
                        <td width="33%" ng-if="Metas_ll.shortname == 'Sharon'"><center><img src="assets/images/Sharon.jpg" width="30%"></td>
                        <td width="33%" ng-if="Metas_ll.shortname == 'Raziel'"><center><img src="assets/images/Raziel.jpg" width="30%"></td>
                        <td width="33%" ng-if="Metas_ll.shortname != 'Ceci' && Metas_ll.shortname != 'Sharon' && Metas_ll.shortname != 'Raziel'">&nbsp;</td>
                        <td width="33%"><center>{{Metas_ll.shortname}} </td>
                        <td width="33%"><center>{{Metas_ll.diar_ll}} </td>
                        <td width="33%"><center>{{Metas_ll.diar_ce}} </td>
                     </tr>
                  </table>
               </div>
               <div class="height-155 animate-show-hide">
                  <canvas class="tc-chart" tc-chartjs-bar chart-options="options" chart-data="post.metas_di" width="100%"></canvas>
               </div>
            </div>
         </div>
      </div>

      <div class="col-md-3">
         <div class="panel panel-white {{wait}}">
            <div class="panel-heading border-light bg-orange">
               <h4 class="panel-title"><b>RANKING CONTACTOS MENSUALES</b></h4>
            </div>
            <div class="panel-body">
               <div class="height-200 animate-show-hide">
                  <table ng-table="tableParamsMetas_ll" show-filter="false" class="table table-condensed table-hover">
                     <tr>
                         <th></th>
                         <th><center>EJECUTIVO</th>
                         <th><center>REAL</th>
                         <th><center>META</th>
                         <th><center>%</th>
                     </tr>
                     <tr ng-repeat="Metas_ll in $data" >
                        <td width="33%" ng-if="Metas_ll.shortname == 'Ceci'">  <center><img src="assets/images/Cecilia.jpg" width="30%"></td>
                        <td width="33%" ng-if="Metas_ll.shortname == 'Sharon'"><center><img src="assets/images/Sharon.jpg" width="30%"></td>
                        <td width="33%" ng-if="Metas_ll.shortname == 'Raziel'"><center><img src="assets/images/Raziel.jpg" width="30%"></td>
                        <td width="33%" ng-if="Metas_ll.shortname != 'Ceci' && Metas_ll.shortname != 'Sharon' && Metas_ll.shortname != 'Raziel'">&nbsp;</td>
                        <td width="33%"><center>{{Metas_ll.shortname}} </td>
                        <td width="33%"><center>{{Metas_ll.real_ll}} </td>
                        <td width="33%"><center>{{Metas_ll.meta_ll}} </td>
                        <td width="33%"><center>{{Metas_ll.efec_ll | number : 2 }} </td>
                     </tr>
                  </table>
               </div>
               <div class="height-155 animate-show-hide">
                  <canvas class="tc-chart" tc-chartjs-bar chart-options="options" chart-data="post.metas_ll" width="100%"></canvas>
               </div>
            </div>
         </div>
      </div>

      <div class="col-md-3">
         <div class="panel panel-white {{wait}}">
            <div class="panel-heading border-light bg-orange">
               <h4 class="panel-title"><b>RANKING CITAS</b></h4>
            </div>
            <div class="panel-body">
               <div class="height-200 animate-show-hide">
                  <table ng-table="tableParamsMetas_ci" show-filter="false" class="table table-condensed table-hover">
                     <tr>
                         <th></th>
                         <th><center>EJECUTIVO</th>
                         <th><center>REAL</th>
                         <th><center>META</th>
                         <th><center>%</th>
                     </tr>
                     <tr ng-repeat="Metas_ci in $data" >
                        <td width="33%" ng-if="Metas_ci.shortname == 'Ceci'">  <center><img src="assets/images/Cecilia.jpg" width="30%"></td>
                        <td width="33%" ng-if="Metas_ci.shortname == 'Sharon'"><center><img src="assets/images/Sharon.jpg" width="30%"></td>
                        <td width="33%" ng-if="Metas_ci.shortname == 'Raziel'"><center><img src="assets/images/Raziel.jpg" width="30%"></td>
                        <td width="33%" ng-if="Metas_ci.shortname != 'Ceci' && Metas_ci.shortname != 'Sharon' && Metas_ci.shortname != 'Raziel'">&nbsp;</td>
                        <td width="33%"><center>{{Metas_ci.shortname}} </td>
                        <td width="33%"><center>{{Metas_ci.real_ci}} </td>
                        <td width="33%"><center>{{Metas_ci.meta_ci}} </td>
                        <td width="33%"><center>{{Metas_ci.efec_ci | number : 2 }} </td>
                     </tr>
                  </table>
               </div>
               <div class="height-155 animate-show-hide">
                  <canvas class="tc-chart" tc-chartjs-bar chart-options="options" chart-data="post.metas_ci" width="100%"></canvas>
               </div>
            </div>
         </div>
      </div>

      <div class="col-md-3">
         <div class="panel panel-white {{wait}}">
            <div class="panel-heading border-light bg-orange">
               <h4 class="panel-title"><b>RANKING MONTO MENSUAL</b></h4>
            </div>
            <div class="panel-body">
               <div class="height-200 animate-show-hide">
                  <table ng-table="tableParamsMetas_m" show-filter="false" class="table table-condensed table-hover">
                     <tr>
                         <th></th>
                         <th><center>EJECUTIVO</th>
                         <th><center>REAL</th>
                         <th><center>META</th>
                         <th><center>%</th>
                     </tr>
                     <tr ng-repeat="Metas_m in $data" >
                        <td width="33%" ng-if="Metas_m.shortname == 'Ceci'">  <center><img src="assets/images/Cecilia.jpg" width="30%"></td>
                        <td width="33%" ng-if="Metas_m.shortname == 'Sharon'"><center><img src="assets/images/Sharon.jpg" width="30%"></td>
                        <td width="33%" ng-if="Metas_m.shortname == 'Raziel'"><center><img src="assets/images/Raziel.jpg" width="30%"></td>
                        <td width="33%" ng-if="Metas_m.shortname != 'Ceci' && Metas_m.shortname != 'Sharon' && Metas_m.shortname != 'Raziel'"></td>

                        <td width="33%"><center>{{Metas_m.shortname}} </td>
                        <td width="33%"><center>{{Metas_m.real_m | currency}} </td>
                        <td width="33%"><center>{{Metas_m.meta_m | currency}} </td>
                        <td width="33%"><center>{{Metas_m.efec_m | number : 2 }} </td>
                     </tr>
                  </table>
               </div>
               <div class="height-155 animate-show-hide">
                  <canvas class="tc-chart" tc-chartjs-bar chart-options="optionscurrency" chart-data="post.metas_m" width="100%"></canvas>
               </div>
            </div>
         </div>
      </div>

   </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-white {{wait}}">
                <div class="panel-heading border-light bg-orange">
                    <h4 class="panel-title"><b>GENERAL EQUIPO</b></h4>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-white {{wait}}">
                <div class="panel-heading border-light bg-orange">
                    <h4 class="panel-title"><b>LLAMADAS MENSUALES</b></h4>
                </div>
                <div class="panel-body">
                    <div class="animate-show-hide">
                        <table width='100%'>
                            <tr>
                                <th width='33%'><center>META</center></th>
                                <th width='33%'><center>REAL</th>
                                <th width='33%'><center>%</th>
                            </tr>
                            <tr>
                                <td><center>{{post.MetasTotal[0].meta_t}}</td>
                                <td><center>{{post.MetasTotal[0].real_t}}</td>
                                <td class='alert alert-success' ng-if="post.MetasTotal[0].real_t*100/post.MetasTotal[0].meta_t >= 50"><center>{{post.MetasTotal[0].real_t*100/post.MetasTotal[0].meta_t | number : 2}}</td>
                                <td class='alert alert-danger'  ng-if="post.MetasTotal[0].real_t*100/post.MetasTotal[0].meta_t <  50"><center>{{post.MetasTotal[0].real_t*100/post.MetasTotal[0].meta_t | number : 2}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-white {{wait}}">
                <div class="panel-heading border-light bg-orange">
                    <h4 class="panel-title"><b>CITAS MENSUALES</b></h4>
                </div>
                <div class="panel-body">
                    <div class="animate-show-hide">
                        <table width='100%'>
                            <tr>
                                <th width='33%'><center>META</th>
                                <th width='33%'><center>REAL</th>
                                <th width='33%'><center>%</th>
                            </tr>
                            <tr>
                                <td><center>{{post.MetasTotal[1].meta_t}}</td>
                                <td><center>{{post.MetasTotal[1].real_t}}</td>
                                <td class='alert alert-success' ng-if="post.MetasTotal[1].real_t*100/post.MetasTotal[1].meta_t >= 50"><center>{{post.MetasTotal[1].real_t*100/post.MetasTotal[1].meta_t | number : 2}}</td>
                                <td class='alert alert-danger'  ng-if="post.MetasTotal[1].real_t*100/post.MetasTotal[1].meta_t <  50"><center>{{post.MetasTotal[1].real_t*100/post.MetasTotal[1].meta_t | number : 2}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-white {{wait}}">
                <div class="panel-heading border-light bg-orange">
                    <h4 class="panel-title"><b>MONTO MENSUAL</b></h4>
                </div>
                <div class="panel-body">
                    <div class="animate-show-hide">
                        <table width='100%'>
                            <tr>
                                <th width='33%'><center>META</th>
                                <th width='33%'><center>REAL</th>
                                <th width='33%'><center>%</th>
                            </tr>
                            <tr>
                                <td><center>{{post.MetasTotal[2].meta_t | currency}}</td>
                                <td><center>{{post.MetasTotal[2].real_t | currency}}</td>
                                <td class='alert alert-success' ng-if="post.MetasTotal[2].real_t*100/post.MetasTotal[2].meta_t >= 50"><center>{{post.MetasTotal[2].real_t*100/post.MetasTotal[2].meta_t | number : 2}}</td>
                                <td class='alert alert-danger'  ng-if="post.MetasTotal[2].real_t*100/post.MetasTotal[2].meta_t <  50"><center>{{post.MetasTotal[2].real_t*100/post.MetasTotal[2].meta_t | number : 2}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

</section>
