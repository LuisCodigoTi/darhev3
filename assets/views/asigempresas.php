<!-- start: PAGE TITLE -->
<style>
.rojo {
    color: #ff6666;
}
.amarillo {
    color: #cccc00;
}
.azul {
    color: #4db8ff;
}
.verde {
    color: #00cc00;
}
.azul2 {
    color: #0000ff;
}
.morado {
    color: #5900b3;
}
.crema {
    color: #ff66ff;
}
.gris {
    color: #d1d1e0;
}
.negro {
    color: #000000;
}
.oro {
    color: #808000;
}
.naranja {
    color: #ff9933;
}
.selected {
    font-weight:bold;
    background-color: #ccffcc;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle">Asignación de Empresas</h1>
			<span class="mainDescription">Sección para administrar la asignación de empresas</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesCiviles -->
<section ng-controller="darhe" ng-init="init('A')">
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
         <p>Para asignar empresas a los colaboradores de daRHe, por favor siga estos pasos:</p>
         <ol>
            <li>Seleccione la pestaña "Exportar"</li>
            <li>Oprima el botón "Exportar a CSV". Con esta acción se descargará un archivo con extensión ".cvs" que puede ser abierto en Excel. La información contenida en ese archivo refleja la asignación actual de las empresas registradas en el sistema. Las columnas que contiene ese archivo son las siguientes:
               <ul>
                  <li><b>fuente</b>. Origen de la empresa. <em>Por favor no edite o cambié este dato.</em></li>
                  <li><b>idempresa</b>. Clave de la empresa. <em>Por favor no edite o cambié este dato.</em></li>
                  <li><b>desempresa</b>. Nombre de la empresa. <em>Por favor no edite o cambié este dato.</em></li>
                  <li><b>equipo</b>. Equipo de colaboradores al que esta asignada esta empresa. Este es el dato que tiene que editarse para asignar la empresa a un nuevo equipo y por consiguiente a un nuevo colaborador. En la sección "Usuarios" se puede consultar y/o modificar el equipo al que pertenece cada usuario del sistema. Si requiere agregar un nuevo equipo es necesario duplicar la fila y asignar el nuevo equipo.</li>
                  <li><b>tipocontacto</b>. Último contacto registrado para esta empresa. <em>Por favor no edite o cambié este dato.</em></li>
                  <li><b>usuariocontacto</b>. Usuario que realizó el último contacto. <em>Por favor no edite o cambié este dato.</em></li>
                  <li><b>$$hashKey</b>. Por favor elimine esta columna, de lo contrario no se cargará el archivo adecuadamente.</li>
               </ul>
            </li>
            <li>Edite el archivo ".csv" y asigne las empresas de acuerdo al criterio deseado.</li>
            <li>Seleccione la pestaña "Importar"</li>
            <li>Espere a que aparezca la información que el sistema está cargando del archivo.</li>
            <li>Guarde la información oprimiendo el botón "Guardar Asignación de Empresas"</li>
         </ol>
         <br><br>
         <tabset class="tabbable">
            <tab heading="Exportar">
               <div class="row">
                  <div class="col-md-12">
                     <br>
                     <p align="center">
                        <button class="btn btn-wide btn-orange" ng-csv="post.Empresas" filename="asignacionempresas.csv" csv-label="true" field-separator="," decimal-separator=".">Exportar a CSV</button>
                     </p><br>
                     <table ng-table="tableParamsPermExp" show-filter="true" class="table table-condensed table-hover">
                        <tr ng-repeat="Empresas in $data" ng-click="detalle(Empresas,$index);" ng-class="{ 'selected':$index == selectedRowE}">
                           <td data-title="'Fuente'" filter="{ 'fuente': 'text' }" sortable="'fuente'" > {{Empresas.fuente}} </td>
                           <td data-title="'ID'" filter="{ 'idempresa': 'text' }" sortable="'idempresa'" > {{Empresas.idempresa}} </td>
                           <td data-title="'Empresa'" filter="{ 'desempresa': 'text' }" sortable="'desempresa'" > {{Empresas.desempresa}} </td>
                           <td data-title="'Equipo'" filter="{ 'equipo': 'text' }" sortable="'equipo'" > {{Empresas.equipo}} </td>
                           <td data-title="'TipoContacto'" filter="{ 'tipocontacto': 'text' }" sortable="'tipocontacto'" > {{Empresas.tipocontacto}} </td>
                           <td data-title="'UsuarioContacto'" filter="{ 'usuariocontacto': 'text' }" sortable="'usuariocontacto'" > {{Empresas.usuariocontacto}} </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </tab>
            <tab heading="Importar">
               <div class="row">
                  <div class="col-md-12">
                     <br>
                     <p align="center">
                        <ng-csv-import content="csv.content" ng-model="lqs" id="lqs"
                           material
                           md-button-class="btn btn-wide btn-orange"
                           md-svg-icon="file:ic_file_upload_24px"
                           md-button-title="Seleccionar Archivo"
                           header="csv.header"
                           separator="csv.separator"
                           result="post.asignarempresas"
                           accept="csv.accept"
                           header-visible>
                        </ng-csv-import>
                     </p><br>
                     <table ng-table="tableParamsPermImp" show-filter="true" class="table table-condensed table-hover">
                        <tr ng-repeat="Empresas in $data" ng-click="detalle(Empresas,$index);" ng-class="{ 'selected':$index == selectedRowE}">
                           <td data-title="'Fuente'" filter="{ 'fuente': 'text' }" sortable="'fuente'" > {{Empresas.fuente}} </td>
                           <td data-title="'ID'" filter="{ 'idempresa': 'text' }" sortable="'idempresa'" > {{Empresas.idempresa}} </td>
                           <td data-title="'Empresa'" filter="{ 'desempresa': 'text' }" sortable="'desempresa'" > {{Empresas.desempresa}} </td>
                           <td data-title="'Equipo'" filter="{ 'equipo': 'text' }" sortable="'equipo'" > {{Empresas.equipo}} </td>
                           <td data-title="'TipoContacto'" filter="{ 'tipocontacto': 'text' }" sortable="'tipocontacto'" > {{Empresas.tipocontacto}} </td>
                           <td data-title="'UsuarioContacto'" filter="{ 'usuariocontacto': 'text' }" sortable="'usuariocontacto'" > {{Empresas.usuariocontacto}} </td>
                        </tr>
                     </table>
                     <br>
                     <p align="center">
                        <button class="btn btn-wide btn-orange" ng-click="guardaasignacionempresas();">Guardar Asignación de Empresas</button>
                     </p><br>
                  </div>
               </div>
         </tab>
         </tabset>
		</div>
	</div>
</section>
<!-- end: CONSULTA DE ExpedientesCiviles -->
