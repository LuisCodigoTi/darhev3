<style>
.rojo {
    color: #ff6666;
}
.amarillo {
    color: #cccc00;
}
.azul {
    color: #4db8ff;
}
.verde {
    color: #00cc00;
}
.azul2 {
    color: #0000ff;
}
.morado {
    color: #5900b3;
}
.crema {
    color: #ff66ff;
}
.gris {
    color: #d1d1e0;
}
.negro {
    color: #000000;
}
.oro {
    color: #808000;
}
.naranja {
    color: #ff9933;
}
.negro {
    color: #000000;
}
.rosa {
    color: #F781F3;
}
.selected {
    font-weight:bold;
    background-color: #ccffcc;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15"  ng-controller="darhe">
	<div class="row">
		<div class="col-sm-7">
			<h1 class="mainTitle">Prospección</h1>
		</div>
		<div class="col-sm-5">
			<!-- start: MINI STATS WITH SPARKLINE -->
			<!-- /// controller:  'SparklineCtrl' -  localtion: assets/js/controllers/dashboardCtrl.js /// -->
			<ul class="mini-stats pull-right">
				<li>
					<div class="sparkline">
						<i class="fa fa-phone fa-2x text-orange" tooltip="Llamadas" tooltip-placement="bottom" ></i>
					</div>
					<div class="values">
						<strong class="text-dark no-margin">{{totales.Llamadas}}</strong>
					</div>
				</li>
				<li>
					<div class="sparkline">
						<i class="fa fa-envelope-o fa-2x text-orange" tooltip="Correos" tooltip-placement="bottom" ></i>
					</div>
					<div class="values">
						<strong class="text-dark no-margin">{{totales.Correos}}</strong>
					</div>
				</li>
				<li>
					<div class="sparkline">
						<i class="fa fa-calendar fa-2x text-orange" tooltip="Citas" tooltip-placement="bottom" ></i>
					</div>
					<div class="values">
						<strong class="text-dark no-margin">{{totales.Citas}}</strong>
					</div>
				</li>
				<li>
					<div class="sparkline">
						<i class="fa fa-thumbs-o-up fa-2x text-orange" tooltip="Contactos Efectivos" tooltip-placement="bottom" ></i>
					</div>
					<div class="values">
						<strong class="text-dark no-margin">{{totales.ContactoEfectivo}}</strong>
					</div>
				</li>
			</ul>
		</div>
	</div>
</section>
<!-- end: PAGE TITLE -->



<!-- start: CONSULTA DE ExpedientesCiviles -->
<section ng-controller="darhe" ng-init="init()">
<div class="modal-header no-radius col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 animate-show-hide" ng-show="actividad">
    <div class="modal-header"  ng-show="actividad">
			<h3 class="modal-title">Registro de Actividad</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="ContactoForm">
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6">
								<label for="tempAccion.tipoaccion">
									Actividad (Campo requerido)
								</label>
			                    <select size=10 class="form-control" ng-model='tempAccion.tipoaccion'>
			                        <option value='Llamada'>Llamada</option>
			                        <option value='Envío de Correo'>Envío de Correo</option>
			                        <option value='Cita agendada'>Cita agendada</option>
			                        <option value='Contacto Efectivo'>Contacto Efectivo</option>
			                    </select>
							</div>
							<div class="col-md-6">
							<div class="form-group">
                                    <label>
                                        Semaforo
                                    </label>
                                    <ui-select ng-model="tempAccion.idestatus" theme="bootstrap" name="idestatus">
                                        <ui-select-match placeholder="Selecciona nuevo estatus...">
                                            {{$select.selected.desestatus}}
                                        </ui-select-match>
                                        <ui-select-choices repeat="item.idestatus as item in estatus | filter: idestatus = '1' ">
                                            <div ng-bind-html="item.desestatus | highlight: $select.search"></div>
                                            <!--<small ng-bind-html="item.descripcion | highlight: $select.search"></small>-->
                                        </ui-select-choices>
                                    </ui-select>
                                </div>
							</div>
						</div>
						<div class="form-group">
							<label for="tempAccion.montocotizado">
								Monto Cotizado
							</label>
							<input type="text" class="form-control" ng-model='tempAccion.montocotizado'>
						</div>
						<div class="form-group">
							<label for="tempAccion.estatusdesglosado">
								Estatus desglosado (Campo requerido)
							</label>
							<textarea rows="4" maxlength="2000" class="form-control" ng-model='tempAccion.estatusdesglosado'></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<tabset class="tabbable">
							<tab heading="Contacto">
								<div class="form-group">
									<label for="tempAccion.descontacto">
										Contacto
									</label>
									<input type="text" class="form-control" ng-model='tempAccion.descontacto'>
								</div>
								<div class="form-group">
									<label for="tempAccion.desfuente">
										Fuente
									</label>
									<input type="text" class="form-control" ng-model='tempAccion.desfuente'>
								</div>
								<div class="form-group">
									<label for="tempAccion.telefono">
										Teléfono
									</label>
									<input type="text" class="form-control" ng-model='tempAccion.telefono'>
								</div>
								<div class="form-group">
									<label for="tempAccion.email">
										Correo Electrónico
									</label>
									<input type="text" class="form-control" ng-model='tempAccion.email'>
								</div>
							</tab>
							<tab heading="Siguiente Contacto">
								<div class="form-group">
									<label>
										Fecha de siguiente contacto
									</label>
									<p class="input-group">
										<input type="text" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempAccion.fechasiguientecontacto" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="opened" date-format="dd/MMM/yyyy" date-type="string" disabled />
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="opened=!opened">
												<i class="glyphicon glyphicon-calendar"></i>
											</button>
										</span>
									</p>
								</div>
								<div class="form-group">
									<label>
										Hora
									</label>
									<timepicker ng-model="tempAccion.fechasiguientecontacto" hour-step="1" minute-step="1" show-meridian="true"></timepicker>
								</div>
							</tab>
							<tab heading="Cita">
								<div class="form-group">
									<label>
										Fecha de cita
									</label>
									<p class="input-group">
										<input type="text" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempAccion.fechacita" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="opened" date-format="dd/MMM/yyyy" date-type="string" disabled />
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="opened=!opened">
												<i class="glyphicon glyphicon-calendar"></i>
											</button>
										</span>
									</p>
								</div>
								<div class="form-group">
									<label>
										Hora
									</label>
									<timepicker ng-model="tempAccion.fechacita" hour-step="1" minute-step="1" show-meridian="true"></timepicker>
								</div>
								<div class="form-group">
									<label for="tempAccion.observacionescita">
										Observaciones
									</label>
									<textarea rows="4" maxlength="2000" class="form-control" ng-model='tempAccion.observacionescita'></textarea>
								</div>
							</tab>
						</tabset>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-warning" ng-click="actividad=false">Cancelar</button>
		<button class="btn btn-success" ng-click="guarda_actividad()">Guardar</button>
	</div>
	</div>

    <div class="panel panel-white no-radius col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 animate-show-hide" ng-show="informacion">
        <div class="panel-body">
            <div class="row margin-bottom-20">
                <div class="col-xs-12">
                    <form role="form" class="margin-bottom-20" name="Form">
					    <div class="form-group">
                            &nbsp;
                            <label>
							  Contacto
                            </label>
                            <input type="text" ng-model="tempContacto.descontacto" name="descontacto" placeholder="Contacto" required maxlength="100" class="form-control" capitalize>
						</div>

                        <div class="form-group">
                            &nbsp;
                            <label>
							  Fuente
                            </label>
                            <input type="text" ng-model="tempContacto.desfuente" name="desfuente" placeholder="Contacto" required maxlength="100" class="form-control" capitalize>
						</div>
					
						<div class="form-group">
                            &nbsp;
                            <label>
							  Telefono
                            </label>
                            <input type="text" ng-model="tempContacto.telefono" name="telefono" placeholder="Telefono" required maxlength="100" class="form-control" capitalize>
						</div>
						
						<div class="form-group">
                            &nbsp;
                            <label>
							  Correo Electronico
                            </label>
                            <input type="text" ng-model="tempContacto.email" name="email" placeholder="Correo Electronico" required maxlength="100" class="form-control" capitalize>
						</div>

						<div class="form-group">
                             &nbsp;
                            <label>
							  ¿Datos Validados?
                            </label>
                            <input type="checkbox" ng-model="tempContacto.verificado" name="verificado" class="form-control">
						</div>
                    </form>
                </div>
            </div>
            <div class="row">
                <button type="button" class="btn btn-primary" ng-click="guarda_contacto()">
                    Guardar
                </button>
				<button type="button" class="btn btn-primary" ng-click="informacion=false">
                    Cancelar
                </button>
            </div>
        </div>
	</div>
	
	<div class="panel panel-white no-radius col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 animate-show-hide" ng-show="asignar">
        <div class="panel-body">
            <div class="row margin-bottom-20">
				<div class="col-xs-12">
                    <form role="form" class="margin-bottom-20" name="Form">
					    <div class="form-group">
                            &nbsp;
                            <label>
							  Contacto
                            </label>
                            <input type="text" ng-model="tempContacto.descontacto" name="descontacto" placeholder="Contacto" required maxlength="100" class="form-control" capitalize>
						</div>

                        <div class="form-group">
                            &nbsp;
                            <label>
							  Fuente
                            </label>
                            <input type="text" ng-model="tempContacto.fuente" name="fuente" placeholder="Fuente" required maxlength="100" class="form-control" capitalize>
						</div>
					
						<div class="form-group">
                            &nbsp;
                            <label >
                                Usuario
                            </label>
                            <ui-select ng-model="tempContacto.usuario" theme="bootstrap" name="usuario" required>
                                <ui-select-match placeholder="Selecciona el usuario ...">
                                    {{$select.selected.name}}
                                </ui-select-match>
                                <ui-select-choices repeat="item.user as item in usuarios | filter: $select.search ">
								    <div ng-bind-html="item.name | highlight: $select.search"></div>
                                    <div ng-bind-html="item.user | highlight: $select.search"></div>
                                </ui-select-choices>
                            </ui-select>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row" style="text-align:right;">
                <button type="button" class="btn btn-primary" ng-click="guarda_usuario()">
                    Guardar
                </button>
				<button type="button" class="btn btn-primary" ng-click="asignar=false">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
	<!---------------------------------------------------------------------------------------------------------------------------------------->
	<!---------------------------------------------------------------------------------------------------------------------------------------->
	<!---------------------------------------------------------------------------------------------------------------------------------------->
	<div class="row" ng-show="!informacion && !actividad && !asignar">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-white {{wait}}">
						<div class="panel-heading border-light bg-orange">
							<h4 class="panel-title"><b>Contactos</b></h4>
							<ul class="panel-heading-tabs border-light">
								<li>
									<div class="pull-right">
										<p align="center">
											<a class="btn btn-xs btn-orange" href="#" ng-click="agrega_contacto()" tooltip="Agregar nuevo Contacto"><i class="fa fa-plus"></i></a>
										</p>
									</div>
								</li>
							</ul>
						</div>
						<div class="panel-body">
								<table ng-table="tableParamsC" class="table table-condensed table-hover">
									<tr ng-repeat="Contactos in contactos" ng-click="contactoHistorial(Contactos,$index);" ng-class="{'selected':$index == selectedRowC}">
									   <td data-title="" sortable="'desestatus'" class="text-center" header-class="'text-center'"> <i class="fa fa-circle" 
									    ng-class="{
								              'negro'   :Contactos.idestatus == 0,
								              'amarillo':Contactos.idestatus == 1,
								              'rojo'    :Contactos.idestatus == 2,
							                  'azul'    :Contactos.idestatus == 3,
								              'verde'   :Contactos.idestatus == 4,
								              'azul2'   :Contactos.idestatus == 5,
								              'morado'  :Contactos.idestatus == 6,
								              'crema'   :Contactos.idestatus == 7,
								              'gris'    :Contactos.idestatus == 8,
								              'negro'   :Contactos.idestatus == 9,
								              'oro'     :Contactos.idestatus == 10,
								              'naranja' :Contactos.idestatus == 11,
								              'rosa'    :Contactos.idestatus == 12}" tooltip="{{Contactos.desestatus}}"></i> </td>
										<td data-title="'Fuente'" sortable="'desfuente'" > {{Contactos.desfuente}} </td>
										<td data-title="'Contacto'" sortable="'descontacto'" > {{Contactos.descontacto}} </td>
										<td data-title="'Teléfono'" sortable="'telefono'"  > {{Contactos.telefono}} </td>
										<td data-title="'Correo Electrónico'" sortable="'email'" > {{Contactos.email}} </td>
			                            <td class="center">
                                            <a href="#" class="btn btn-transparent btn-md" ng-click="edita_contacto(Contactos)"     tooltip="Actualizar datos del Contacto"><i class="fa fa-pencil"></i></a>
											<a href="#" class="btn btn-transparent btn-md" ng-click="registra_actividad(Contactos)" tooltip="Registrar actividad"><i class="fa fa-thumbs-o-up"></i></a>
											<a href="#" class="btn btn-transparent btn-md" ng-click="asignar_usuario(Contactos)"    tooltip="Asignar usuario" ng-show="user.admin === 'SI'"><i class="fa fa-user"></i></a>
			                            </td>
									</tr>
								</table>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>


		
		<div class="col-md-12">
					<div class="panel panel-white {{wait}}">
						<div class="panel-heading border-light bg-orange">
							<h4 class="panel-title"><b>Historial de Contacto</b></h4>
						</div>
						<div class="panel-body">
								<table ng-table="tableParamsH" class="table table-condensed table-hover">
									<tr ng-repeat="Historial in datoscontactos">
										<td data-title="'Usuario'" sortable="'user'" > {{Historial.shortname}} </td>
										<td data-title="'Actividad'" sortable="'tipoaccion'" > {{Historial.tipoaccion}} </td>
										<td data-title="'Fecha y Hora'" sortable="'fechaaccion'" > {{Historial.fechaaccion}} </td>
										<td data-title="'Contacto'" sortable="'descontacto'" > {{Historial.descontacto}} </td>
										<td data-title="'Estatus Desglosado'" sortable="'estatusdesglosado'" > {{Historial.estatusdesglosado}} </td>
										<td data-title="'Semaforo'" sortable="'idestatus'" class="text-center" header-class="'text-center'"> <i class="fa fa-circle" 
											ng-class="{
												'negro'   :Historial.idestatus == 0,
												'amarillo':Historial.idestatus == 1,
												'rojo'    :Historial.idestatus == 2,
												'azul'    :Historial.idestatus == 3,
												'verde'   :Historial.idestatus == 4,
												'azul2'   :Historial.idestatus == 5,
												'morado'  :Historial.idestatus == 6,
												'crema'   :Historial.idestatus == 7,
												'gris'    :Historial.idestatus == 8,
												'negro'   :Historial.idestatus == 9,
												'oro'     :Historial.idestatus == 10,
												'naranja' :Historial.idestatus == 11,
												'rosa'    :Historial.idestatus == 12}" ng-if="Historial.idestatus!=0" tooltip="{{Historial.desestatus}}"></i> </td>
			                            <td class="center">
                                            <a href="#" class="btn btn-transparent btn-md" ng-click="visualizar_actividad_contacto(Historial)" tooltip="Visualizar actividad"><i class="fa fa-eye"></i></a>
			                            </td>
									</tr>
								</table>
						</div>
					</div>
				</div>
		</div>
		
	<div class="row" ng-show="!informacion && !actividad && !asignar">
		<div class="col-md-4">
			<div class="panel panel-white {{wait}}">
				<div class="panel-heading border-light bg-orange">
					<h4 class="panel-title"><b>Agenda para hoy</b></h4>
				</div>
				<div class="panel-body">
						<table ng-table="tableParamsA" show-filter="false" class="table table-condensed table-hover">
							<tr ng-repeat="Agenda in agenda" >
								<td data-title="'Contacto'" filter="{ 'descontacto': 'text' }" sortable="'descontacto'" > {{Agenda.descontacto}} </td>
								<td data-title="'Siguiente Contacto'" filter="{ 'fechasiguientecontacto': 'text' }" sortable="'fechasiguientecontacto'" > {{Agenda.fechasiguientecontacto}} </td>
							</tr>
						</table>
				</div>
			</div>
		</div>
		
		<div class="col-md-8">
			<div class="panel panel-white {{wait}}">
				<div class="panel-heading border-light bg-orange">
					<h4 class="panel-title"><b>Historial de actividades realizadas hoy</b></h4>
				</div>
				<div class="panel-body">
						<table ng-table="tableParamsHh" class="table table-condensed table-hover">
							<tr ng-repeat="Historial in historiaHoy">
								<td data-title="'Actividad'" sortable="'tipoaccion'" > {{Historial.tipoaccion}} </td>
								<td data-title="'Fecha y Hora'" sortable="'fechaaccion'" > {{Historial.fechaaccion}} </td>
								<td data-title="'Contacto'" sortable="'user'" > {{Historial.descontacto}} </td>
								<td data-title="'Estatus Desglosado'" sortable="'estatusdesglosado'" > {{Historial.estatusdesglosado}} </td>
								<td data-title="'Semaforo'" sortable="'idestatus'" class="text-center" header-class="'text-center'"> <i class="fa fa-circle" 
									ng-class="{
										'negro'   :Historial.idestatus == 0,
										'amarillo':Historial.idestatus == 1,
										'rojo'    :Historial.idestatus == 2,
										'azul'    :Historial.idestatus == 3,
										'verde'   :Historial.idestatus == 4,
										'azul2'   :Historial.idestatus == 5,
										'morado'  :Historial.idestatus == 6,
										'crema'   :Historial.idestatus == 7,
										'gris'    :Historial.idestatus == 8,
										'negro'   :Historial.idestatus == 9,
										'oro'     :Historial.idestatus == 10,
										'naranja' :Historial.idestatus == 11,
										'rosa'    :Historial.idestatus == 12}" ng-if="Historial.idestatus!=0" tooltip="{{Historial.desestatus}}"></i> </td>
	                            <td class="center">
                                    <a href="#" class="btn btn-transparent btn-md" ng-click="visualizar_actividad(Historial)" tooltip="Visualizar actividad"><i class="fa fa-eye"></i></a>
	                            </td>
							</tr>
						</table>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: CONSULTA DE ExpedientesCiviles -->
