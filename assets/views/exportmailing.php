<!-- start: PAGE TITLE -->
<style>
.rojo {
    color: #ff6666;
}
.amarillo {
    color: #cccc00;
}
.azul {
    color: #4db8ff;
}
.verde {
    color: #00cc00;
}
.azul2 {
    color: #0000ff;
}
.morado {
    color: #5900b3;
}
.crema {
    color: #ff66ff;
}
.gris {
    color: #d1d1e0;
}
.negro {
    color: #000000;
}
.oro {
    color: #808000;
}
.naranja {
    color: #ff9933;
}
.selected {
    font-weight:bold;
    background-color: #ccffcc;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle">Asignación de Empresas</h1>
			<span class="mainDescription">Sección para administrar la asignación de empresas</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesCiviles -->
<section ng-controller="darhe" ng-init="init('M')">
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
         <div class="row">
            <div class="col-md-12">
               <br>
               <p align="center">
                  <button class="btn btn-wide btn-orange" ng-csv="post.Empresas" filename="basemailing.csv" csv-label="true" field-separator="," decimal-separator=".">Exportar a CSV</button>
               </p><br>
               <table ng-table="tableParamsExpMail" show-filter="true" class="table table-condensed table-hover">
                  <tr ng-repeat="Empresas in $data" ng-click="detalle(Empresas,$index);" ng-class="{ 'selected':$index == selectedRowE}">
                     <td data-title="'Fuente'" filter="{ 'Fuente': 'text' }" sortable="'Fuente'" > {{Empresas.Fuente}} </td>
                     <td data-title="'Empresa'" filter="{ 'Empresa': 'text' }" sortable="'Empresa'" > {{Empresas.Empresa}} </td>
                     <td data-title="'Tocada'" filter="{ 'Tocada': 'text' }" sortable="'Tocada'" > {{Empresas.Tocada}} </td>
                     <td data-title="'Contacto'" filter="{ 'Contacto': 'text' }" sortable="'Contacto'" > {{Empresas.Contacto}} </td>
                     <td data-title="'Email'" filter="{ 'Email': 'text' }" sortable="'Email'" > {{Empresas.Email}} </td>
                  </tr>
               </table>
            </div>
         </div>
		</div>
	</div>
</section>
<!-- end: CONSULTA DE ExpedientesCiviles -->
