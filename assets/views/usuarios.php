<!-- start: PAGE TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Catálogo de Usuarios</h1>
			<span class="mainDescription">Sección para administrar (Altas, bajas, modificaciones) el catálogo de Usuarios</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE Usuarios -->
<section ng-controller="ngTableCtrl_Usuarios" ng-init="init()">
	<script type="text/ng-template" id="EditarUsuario.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Usuario</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="usuarioForm">
				<label>
					Usuario
				</label>
				<input type="text" class="form-control" id="user" ng-model='tempUsuario.user' ng-readonly="editMode">
				<label>
					Contraseña
				</label>
				<input type="text" class="form-control" id="password" ng-model='tempUsuario.password'>
				<label>
					Nombre Completo
				</label>
				<input type="text" class="form-control" id="name" ng-model='tempUsuario.name'>
				<label>
					Nombre Corto
				</label>
				<input type="text" class="form-control" id="shortname" ng-model='tempUsuario.shortname'>
				<div>
					<label>
						Turno
					</label>
					<select class="form-control" ng-model='tempUsuario.turno'>
						<option value=''></option>
						<option value='Matutino'>Matutino</option>
						<option value='Vespertino'>Vespertino</option>
					</select>
				</div>
				<div>
					<label>
						Permisos de Prospección?
					</label>
					<select class="form-control" ng-model='tempUsuario.invpros' ng-disabled="user.user!='codigoti'">
						<option value='SI'>SI</option>
						<option value='NO'>NO</option>
					</select>
					{{user.user}}
				</div>
				<div>
					<label>
						¿Permisos de Administración?
					</label>
					<select class="form-control" ng-model='tempUsuario.admin' ng-disabled="user.user!='codigoti'">
						<option value='SI'>SI</option>
						<option value='NO'>NO</option>
					</select>
				</div>
				<div>
					<label>
						¿Permisos al Tablero de Control?
					</label>
					<select class="form-control" ng-model='tempUsuario.tablerocontrol' ng-disabled="user.user!='codigoti'">
						<option value='SI'>SI</option>
						<option value='NO'>NO</option>
					</select>
				</div>
				<label>
					Equipo
				</label>
				<input type="text" class="form-control" id="name" ng-model='tempUsuario.equipo'>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="container-fluid container-fullw">
				<div class="row">
					<div class="table-responsive">
						<h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de Usuarios</span></h5>
						<!-- /// controller:  'ngTableCtrl_Turnos' -  localtion: assets/js/controllers/ngTableCtrl_Turnos.js /// -->
						<div>
							<input type="hidden" id="base_path" value="<?php echo BASE_PATH; ?>"/>
							<table ng-table="tableParams" show-filter="true" class="table table-striped table-condensed table-hover">
								<tr ng-repeat="Usuarios in $data">
									<td data-title="'Usuario'" filter="{ 'user': 'text' }" sortable="'user'"> {{Usuarios.user}} </td>
									<td data-title="'Contraseña'" filter="{ 'password': 'text' }" sortable="'password'"> {{Usuarios.password}} </td>
									<td data-title="'Nombre Completo'" filter="{ 'name': 'text' }" sortable="'name'"> {{Usuarios.name}} </td>
									<td data-title="'Nombre Corto'" filter="{ 'shortname': 'text' }" sortable="'shortname'"> {{Usuarios.shortname}} </td>
									<td data-title="'Turno'" filter="{ 'turno': 'text' }" sortable="'turno'"> {{Usuarios.turno}} </td>
									<td data-title="'Permiso de Prospección'" filter="{ 'invpros': 'text' }" sortable="'invpros'"> {{Usuarios.invpros}} </td>
									<td data-title="'Permiso de Administración'" filter="{ 'admin': 'text' }" sortable="'admin'"> {{Usuarios.admin}} </td>
									<td data-title="'Permiso al Tablero de Control'" filter="{ 'tablerocontrol': 'text' }" sortable="'tablerocontrol'"> {{Usuarios.tablerocontrol}} </td>
									<td data-title="'Equipo'" filter="{ 'equipo': 'text' }" sortable="'equipo'"> {{Usuarios.equipo}} </td>
									<td class="center">
										<div class="visible-md visible-lg hidden-sm hidden-xs">
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(Usuarios,true)"><i class="fa fa-pencil"></i></a>
										</div>
										<div class="visible-xs visible-sm hidden-md hidden-lg">
											<div class="btn-group" dropdown is-open="status.isopen">
												<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
													<i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
												</button>
												<ul class="dropdown-menu pull-right dropdown-light" role="menu">
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(Usuarios,true)"><i class="fa fa-pencil"></i> Modificar</a>
														<a href="#">
															Modificar
														</a>
													</li>
												</ul>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<p align="center">
						<a class="btn btn-wide btn-orange" href="#" ng-click="open(Usuarios,false)"><i class="fa fa-plus"></i> Agregar nuevo Usuario</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end: BANDEJA DE ENTRADA DE TURNOS -->
</section>
