<!-- start: PAGE TITLE -->
<style>
.rojo {
    color: #ff6666;
}
.amarillo {
    color: #cccc00;
}
.azul {
    color: #4db8ff;
}
.verde {
    color: #00cc00;
}
.azul2 {
    color: #0000ff;
}
.morado {
    color: #5900b3;
}
.crema {
    color: #ff66ff;
}
.gris {
    color: #d1d1e0;
}
.negro {
    color: #000000;
}
.oro {
    color: #808000;
}
.naranja {
    color: #ff9933;
}
.selected {
    font-weight:bold;
    background-color: #ccffcc;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15"  ng-controller="operaciondiaria">
	<div class="row">
		<div class="col-sm-7">
			<h1 class="mainTitle">Tablero de Control</h1>
			<span class="mainDescription">Operación Diaria</span>
		</div>
		<div class="col-sm-5">
			<!-- start: MINI STATS WITH SPARKLINE -->
			<!-- /// controller:  'SparklineCtrl' -  localtion: assets/js/controllers/dashboardCtrl.js /// -->
			<ul class="mini-stats pull-right">
				<li>
					<div class="sparkline">
						<i class="fa fa-phone fa-2x text-orange" tooltip="Llamadas" tooltip-placement="bottom" ></i>
					</div>
					<div class="values">
						<strong class="text-dark no-margin">{{totales.Llamadas}}</strong>
					</div>
				</li>
				<li>
					<div class="sparkline">
						<i class="fa fa-envelope-o fa-2x text-orange" tooltip="Correos" tooltip-placement="bottom" ></i>
					</div>
					<div class="values">
						<strong class="text-dark no-margin">{{totales.Correos}}</strong>
					</div>
				</li>
				<li>
					<div class="sparkline">
						<i class="fa fa-calendar fa-2x text-orange" tooltip="Citas" tooltip-placement="bottom" ></i>
					</div>
					<div class="values">
						<strong class="text-dark no-margin">{{totales.Citas}}</strong>
					</div>
				</li>
				<li>
					<div class="sparkline">
						<i class="fa fa-building-o fa-2x text-orange" tooltip="Visitas" tooltip-placement="bottom" ></i>
					</div>
					<div class="values">
						<strong class="text-dark no-margin">{{totales.Visitas}}</strong>
					</div>
				</li>
				<li>
					<div class="sparkline">
						<i class="fa fa-question fa-2x text-orange" tooltip="Otros" tooltip-placement="bottom" ></i>
					</div>
					<div class="values">
						<strong class="text-dark no-margin">{{totales.Otros}}</strong>
					</div>
				</li>
			</ul>
			<!-- end: MINI STATS WITH SPARKLINE -->
		</div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesCiviles -->
<section ng-controller="operaciondiaria" ng-init="init()">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-white {{wait}}">
				<div class="panel-heading border-light bg-orange">
					<h4 class="panel-title"><b>Agenda para hoy</b></h4>
				</div>
				<div class="panel-body">
						<table ng-table="tableParamsA" show-filter="false" class="table table-condensed table-hover">
							<tr ng-repeat="Agenda in agenda" >
								<td data-title="'Contacto'" filter="{ 'descontacto': 'text' }" sortable="'descontacto'" > {{Agenda.descontacto}} </td>
								<td data-title="'Siguiente Contacto'" filter="{ 'fechasiguientecontacto': 'text' }" sortable="'fechasiguientecontacto'" > {{Agenda.fechasiguientecontacto}} </td>
							</tr>
						</table>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-white {{wait}}">
				<div class="panel-heading border-light bg-orange">
					<h4 class="panel-title"><b>Historial de actividades realizadas hoy</b></h4>
				</div>
				<div class="panel-body">
						<table ng-table="tableParamsHh" class="table table-condensed table-hover">
							<tr ng-repeat="Historial in $data">
								<td data-title="'Agente'" sortable="'shortname'" > {{Historial.name}} </td>
								<td data-title="'Actividad'" sortable="'tipoaccion'" > {{Historial.tipoaccion}} </td>
								<td data-title="'Fecha y Hora'" sortable="'fechaaccion'" > {{Historial.fechaaccion}} </td>
								<td data-title="'Contacto'" sortable="'user'" > {{Historial.descontacto}} </td>
								<td data-title="'Estatus Desglosado'" sortable="'estatusdesglosado'" > {{Historial.estatusdesglosado}} </td>
								<td data-title="'Semaforo'" sortable="'idestatus'" class="text-center" header-class="'text-center'"> <i class="fa fa-circle" 
									ng-class="{'amarillo':Historial.idestatus == 1,
								'rojo'    :Historial.idestatus == 2,
								'azul'    :Historial.idestatus == 3,
								'verde'   :Historial.idestatus == 4,
								'azul2'   :Historial.idestatus == 5,
								'morado'  :Historial.idestatus == 6,
								'crema'   :Historial.idestatus == 7,
								'gris'    :Historial.idestatus == 8,
								'negro'   :Historial.idestatus == 9,
								'oro'     :Historial.idestatus == 10,
								'naranja' :Historial.idestatus == 11}" ng-if="Historial.idestatus!=0" tooltip="{{Historial.desestatus}}"></i> </td>
	                            <td class="center">
                                    <a href="#" class="btn btn-transparent btn-md" ng-click="open_accion(Historial,true,'lg')" tooltip="Visualizar actividad"><i class="fa fa-eye"></i></a>
	                            </td>
							</tr>
						</table>
				</div>
			</div>
		</div>
	</div>

</section>
<!-- end: CONSULTA DE ExpedientesCiviles -->
