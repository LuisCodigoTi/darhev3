<!-- start: PAGE TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Registro de Metas</h1>
			<span class="mainDescription">Sección para administrar las Metas</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE Usuarios -->
<section ng-controller="metas" ng-init="init()">
	
<div class="modal-header no-radius col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 animate-show-hide" ng-show="editarmetas">
    <div class="modal-header"  ng-show="editarmetas">
			<h3 class="modal-title">Editar Usuario</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="ContactoForm">
			    <div class="form-group">
                  &nbsp;
                  <label>
				    Nombre Completo
                  </label>
                  <input type="text" ng-model="tempMetas.name" name="name" class="form-control">
				</div>
				
				<div class="form-group">
                  &nbsp;
                  <label>
				    Monto Mensual
                  </label>
                  <input type="number" min="0.00" step="0.01" ng-model="tempMetas.monto" name="monto" class="form-control">
				</div>

				<div class="form-group">
                  &nbsp;
                  <label>
				    Llamadas Semanales
                  </label>
                  <input type="number" min="0.00" step="0.01" ng-model="tempMetas.llamadas" name="llamadas" class="form-control">
				</div>
				
				<div class="form-group">
                  &nbsp;
                  <label>
				    Correos Semanales
                  </label>
                  <input type="number" min="0.00" step="0.01" ng-model="tempMetas.correos" name="correos" class="form-control">
				</div>

				<div class="form-group">
                  &nbsp;
                  <label>
				    Visitas Mensuales
                  </label>
                  <input type="number" min="0.00" step="0.01" ng-model="tempMetas.visitas" name="visitas" class="form-control">
				</div>

				<div class="form-group">
                  &nbsp;
                  <label>
				    Citas Semanales
                  </label>
                  <input type="number" min="0.00" step="0.01" ng-model="tempMetas.citas" name="citas" class="form-control">
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-warning" ng-click="editarmetas=false">Cancelar</button>
		<button class="btn btn-success" ng-click="savemetas()">Guardar</button>
	</div>
</div>


	<div class="panel panel-white {{wait}}" ng-show="!editarmetas">
		<div class="panel-body">
			<div class="container-fluid container-fullw">
				<div class="row">
					<div class="table-responsive">
						<h5 class="over-title margin-bottom-15"><span class="text-bold">Metas</span></h5>
						<div>
							<input type="hidden" id="base_path" value="<?php echo BASE_PATH; ?>"/>
							<table ng-table="tableParams" show-filter="true" class="table table-striped table-condensed table-hover">
								<tr ng-repeat="Metas in resmetas">
									<td data-title="'Usuario'" filter="{ 'name': 'text' }" sortable="'name'"> {{Metas.name}} </td>
                                    <td data-title="'Monto Mensual'" filter="{ 'monto': 'text' }" sortable="'monto'"> {{Metas.monto}} </td>
                                    <td data-title="'Llamadas Semanales'" filter="{ 'llamadas': 'text' }" sortable="'llamadas'"> {{Metas.llamadas}} </td>
                                    <td data-title="'Correos Semanales'" filter="{ 'correos': 'text' }" sortable="'correos'"> {{Metas.correos}} </td>
                                    <td data-title="'Visitas Mensuales'" filter="{ 'visitas': 'text' }" sortable="'visitas'"> {{Metas.visitas}} </td>
                                    <td data-title="'Citas Semanales'" filter="{ 'citas': 'text' }" sortable="'citas'"> {{Metas.citas}} </td>
									<td class="center">
										<div class="visible-md visible-lg hidden-sm hidden-xs">
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="edita_metas(Metas)"><i class="fa fa-pencil"></i></a>
										</div>
										<div class="visible-xs visible-sm hidden-md hidden-lg">
											<div class="btn-group" dropdown is-open="status.isopen">
												<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
													<i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
												</button>
												<ul class="dropdown-menu pull-right dropdown-light" role="menu">
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="edita_metas(Metas)"><i class="fa fa-pencil"></i> Modificar</a>
													</li>
												</ul>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end: BANDEJA DE ENTRADA DE TURNOS -->
</section>
