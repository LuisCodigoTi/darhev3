<!-- start: PAGE TITLE -->
<style>
.rojo {
    color: #ff6666;
}
.amarillo {
    color: #cccc00;
}
.azul {
    color: #4db8ff;
}
.verde {
    color: #00cc00;
}
.azul2 {
    color: #0000ff;
}
.morado {
    color: #5900b3;
}
.crema {
    color: #ff66ff;
}
.gris {
    color: #d1d1e0;
}
.negro {
    color: #000000;
}
.oro {
    color: #808000;
}
.naranja {
    color: #ff9933;
}
.selected {
    font-weight:bold;
    background-color: #ccffcc;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle">ABC Contactos</h1>
			<span class="mainDescription">Sección para administrar (Altas y modificaciones) la Base de Datos de Contactos</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesCiviles -->
<section ng-controller="abcontactos" ng-init="init()">
	
<div class="panel panel-white no-radius col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 animate-show-hide" ng-show="editar">    
    <div class="modal-header">
		<h3 class="modal-title">Editar Contacto</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="EmpresaForm">
                <div class="form-group">
                    &nbsp;
                    <label>
					  Contacto
                    </label>
                    <input type="text" ng-model="tempContacto.descontacto" name="descontacto" placeholder="Contacto" required maxlength="100" class="form-control" capitalize>
                </div>
                
                <div class="form-group">
                    &nbsp;
                    <label>
					  Fuente
                    </label>
                    <input type="text" ng-model="tempContacto.desfuente" name="desfuente" placeholder="Fuente" required maxlength="100" class="form-control" capitalize>
                </div>
                
                <div class="form-group">
                    &nbsp;
                    <label>
					  Telefono
                    </label>
                    <input type="text" ng-model="tempContacto.telefono" name="telefono" placeholder="Telefono" required maxlength="100" class="form-control" capitalize>
                </div>

                <div class="form-group">
                    &nbsp;
                    <label>
					  Correo Electronico
                    </label>
                    <input type="text" ng-model="tempContacto.email" name="email" placeholder="Correo Electronico" required maxlength="100" class="form-control" capitalize>
                </div>
                                
			</form>
		</div>
		<div class="row" style="text-align:right;">
                <button type="button" class="btn btn-primary" ng-click="guarda_contacto()">
                    Guardar
                </button>
				<button type="button" class="btn btn-primary" ng-click="editar=false">
                    Cancelar
                </button>
        </div>
</div>    
    

<div class="row" ng-show="!editar">
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<br>
					<p align="center">
						<a class="btn btn-wide btn-orange" href="#" ng-click="agrega_contacto()"><i class="fa fa-plus"></i> Agregar nuevo Contacto</a>
					</p><br>
					<table ng-table="tableParams" show-filter="true" class="table table-condensed table-hover">
						<tr ng-repeat="Contactos in contactos">
							<td data-title="'Contacto'" filter="{ 'descontacto': 'text' }" sortable="'descontacto'" > {{Contactos.descontacto}} </td>
							<td data-title="'Fuente'" filter="{ 'fuente': 'text' }" sortable="'fuente'" > {{Contactos.desfuente}} </td>
                            <td data-title="'Telefono'" filter="{ 'telefono': 'text' }" sortable="'telefono'" > {{Contactos.telefono}} </td>
                            <td data-title="'email'" filter="{ 'email': 'text' }" sortable="'email'" > {{Contactos.email}} </td>
                            <td class="center">
                                <a href="#" class="btn btn-transparent btn-md" ng-click="editar_contacto(Contactos)" tooltip="Modificar Contacto"><i class="fa fa-pencil"></i></a>
                            </td>
						</tr>
					</table>
				</div>
			</div>
		</div>
    </div>
</div>

</section>
<!-- end: CONSULTA DE ExpedientesCiviles -->
