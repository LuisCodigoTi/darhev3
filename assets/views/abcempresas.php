<!-- start: PAGE TITLE -->
<style>
.rojo {
    color: #ff6666;
}
.amarillo {
    color: #cccc00;
}
.azul {
    color: #4db8ff;
}
.verde {
    color: #00cc00;
}
.azul2 {
    color: #0000ff;
}
.morado {
    color: #5900b3;
}
.crema {
    color: #ff66ff;
}
.gris {
    color: #d1d1e0;
}
.negro {
    color: #000000;
}
.oro {
    color: #808000;
}
.naranja {
    color: #ff9933;
}
.selected {
    font-weight:bold;
    background-color: #ccffcc;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle">ABC Empresas</h1>
			<span class="mainDescription">Sección para administrar (Altas y modificaciones) la Base de Datos de Empresas</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesCiviles -->
<section ng-controller="darhe" ng-init="init('P')">
	<script type="text/ng-template" id="EditarEmpresa.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Empresa</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="EmpresaForm">
				<div>
					<label for="tempEmpresa.desempresa">
						Empresa
					</label>
					<input type="text" class="form-control" id="desempresa" ng-model='tempEmpresa.desempresa'>
				</div>
				<div>
					<label for="tempEmpresa.fuente">
						Fuente
					</label>
					<input type="text" class="form-control" id="fuente" ng-model='tempEmpresa.fuente'>
				</div>
				<div>
					<label for="tempEmpresa.equipo">
						Equipo
					</label>
					<input type="text" class="form-control" id="equipo" ng-model='tempEmpresa.equipo'>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<br>
					<p align="center">
						<a class="btn btn-wide btn-orange" href="#" ng-click="openE(Empresas,false,false)"><i class="fa fa-plus"></i> Agregar nueva Empresa</a>
					</p><br>
					<table ng-table="tableParamsE" show-filter="true" class="table table-condensed table-hover">
						<tr ng-repeat="Empresas in $data" ng-click="detalle(Empresas,$index);" ng-class="{ 'selected':$index == selectedRowE}">
							<td data-title="'Empresa'" filter="{ 'desempresa': 'text' }" sortable="'desempresa'" > {{Empresas.desempresa}} </td>
							<td data-title="'Fuente'" filter="{ 'fuente': 'text' }" sortable="'fuente'" > {{Empresas.fuente}} </td>
							<td data-title="'Equipo'" filter="{ 'equipo': 'text' }" sortable="'equipo'" > {{Empresas.equipo}} </td>
                            <td class="center">
                                <a href="#" class="btn btn-transparent btn-md" ng-click="openE(Empresas,true,false)" tooltip="Modificar Empresa"><i class="fa fa-pencil"></i></a>
                            </td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: CONSULTA DE ExpedientesCiviles -->
