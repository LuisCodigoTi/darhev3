<?php
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
      case "savecontacto":
	        savecontacto($mysqli);
			break;
	  case "saveactividad":
	        saveactividad($mysqli);
			break;
	  case "saveusuario":
	        saveusuario($mysqli);
		    break;		
	  case "getcontactos":
            getcontactos($mysqli);
			break;
	  case "gethistorial":
	        gethistorial($mysqli);
			break;	 
	  case "getestatus":
	        getestatus($mysqli);
			break;	 
	  case "getagenda":
	        getagenda($mysqli);
			break;	 
	  case "gethistorialdia":
			gethistorialdia($mysqli);
	  case "getaccionesdiarias":
			getaccionesdiarias($mysqli);
	  case "getusuarios":
	        getusuarios($mysqli);
	  default:
	     invalidRequest();
	}
}else{
	invalidRequest();
}

function saveactividad($mysqli){
	$data = array();
 
	try{
	   $idaccion            = $mysqli->real_escape_string(isset( $_POST['accion']['idaccion'] )           ? $_POST['accion']['idaccion']               : '');
	   $idcontacto         = $mysqli->real_escape_string(isset( $_POST['accion']['idcontacto'] )           ? $_POST['accion']['idcontacto']            : '');
	   $tipoaccion          = $mysqli->real_escape_string(isset( $_POST['accion']['tipoaccion'] )            ? $_POST['accion']['tipoaccion']             : '');
	   $fechaaccion         = $mysqli->real_escape_string(isset( $_POST['accion']['fechaaccion'] )           ? $_POST['accion']['fechaaccion']            : '');
	   $user                = $mysqli->real_escape_string(isset( $_POST['accion']['user'] )               ? $_POST['accion']['user']                   : '');
	   $descontacto         = $mysqli->real_escape_string(isset( $_POST['accion']['descontacto'] )           ? $_POST['accion']['descontacto']            : '');
	   $puesto              = $mysqli->real_escape_string(isset( $_POST['accion']['desfuente'] )                ? $_POST['accion']['desfuente']              : '');
	   $telefono            = $mysqli->real_escape_string(isset( $_POST['accion']['telefono'] )           ? $_POST['accion']['telefono']               : '');
	   $email               = $mysqli->real_escape_string(isset( $_POST['accion']['email'] )              ? $_POST['accion']['email']               : '');
	   $direccion           = $mysqli->real_escape_string(isset( $_POST['accion']['direccion'] )             ? $_POST['accion']['direccion']           : '');
	   $fechasiguientecontacto = $mysqli->real_escape_string(isset( $_POST['accion']['fechasiguientecontacto'] )   ? $_POST['accion']['fechasiguientecontacto']    : '');
	   $fechacita              = $mysqli->real_escape_string(isset( $_POST['accion']['fechacita'] )             ? $_POST['accion']['fechacita']           : '');
	   $observacionescita      = $mysqli->real_escape_string(isset( $_POST['accion']['observacionescita'] )     ? $_POST['accion']['observacionescita']      : '');
	   $montocotizado          = $mysqli->real_escape_string(isset( $_POST['accion']['montocotizado'] )         ? $_POST['accion']['montocotizado']          : '');
	   $estatusdesglosado      = $mysqli->real_escape_string(isset( $_POST['accion']['estatusdesglosado'] )     ? $_POST['accion']['estatusdesglosado']      : '');
	   $idestatus              = $mysqli->real_escape_string(isset( $_POST['accion']['idestatus'] )             ? $_POST['accion']['idestatus']           : '');
 
	   if($tipoaccion == '' || $estatusdesglosado == ''){
		  throw new Exception( "Campos requeridos faltantes" );
	   }
	   
	   $camposainsertar = "(tipoaccion,fechaaccion,user,estatusdesglosado";
 
	   if($descontacto != '') $camposainsertar = $camposainsertar . ',descontacto';
	   if($puesto != '') $camposainsertar = $camposainsertar . ',desfuente';
	   if($telefono != '') $camposainsertar = $camposainsertar . ',telefono';
	   if($email != '') $camposainsertar = $camposainsertar . ',email';
	   if($fechasiguientecontacto != '') $camposainsertar = $camposainsertar . ',fechasiguientecontacto';
	   if($fechacita != '') $camposainsertar = $camposainsertar . ',fechacita';
	   if($observacionescita != '') $camposainsertar = $camposainsertar . ',observacionescita';
	   if($montocotizado != '') $camposainsertar = $camposainsertar . ',montocotizado';
	   if($idestatus != '') $camposainsertar = $camposainsertar . ',idestatus';
 
	   $camposainsertar = $camposainsertar . ')';
 
	   $valoresdecampos = "('$tipoaccion',NOW(),'$user','$estatusdesglosado'";
 
	   if($descontacto != '') $valoresdecampos = $valoresdecampos . ",'$descontacto'";
	   if($puesto != '') $valoresdecampos = $valoresdecampos . ",'$puesto'";
	   if($telefono != '') $valoresdecampos = $valoresdecampos . ",'$telefono'";
	   if($email != '') $valoresdecampos = $valoresdecampos . ",'$email'";
	   if($fechasiguientecontacto != '') $valoresdecampos = $valoresdecampos . ",'$fechasiguientecontacto'";
	   if($fechacita != '') $valoresdecampos = $valoresdecampos . ",'$fechacita'";
	   if($observacionescita != '') $valoresdecampos = $valoresdecampos . ",'$observacionescita'";
	   if($montocotizado != '') $valoresdecampos = $valoresdecampos . ",$montocotizado";
	   if($idestatus != '') $valoresdecampos = $valoresdecampos . ",$idestatus";
 
	   $valoresdecampos = $valoresdecampos . ')';
 
	   $query = "INSERT INTO accionesrealizadas " . $camposainsertar . " VALUES " . $valoresdecampos;
 
	   if($idestatus!='') 
		  $query = $query . " ; update contactos set idestatus = $idestatus where idcontacto = $idcontacto";
	   if($fechasiguientecontacto!='') 
		  $query = $query . " ; update contactos set fecsigcont = '$fechasiguientecontacto' where idcontacto = $idcontacto";
 
	   if( $mysqli->multi_query( $query ) ){
		  $data['success'] = true;
		  $data['message'] = 'Actividad insertada exitosamente.';
		  $data['idaccion'] = (int) $mysqli->insert_id;
	   }else{
		  throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
	   }
	   $mysqli->close();
	   echo json_encode($data);
	   exit;
	}catch (Exception $e){
	   $data['success'] = false;
	   $data['message'] = $e->getMessage();
	   echo json_encode($data);
	   exit;
	}
 }

function savecontacto($mysqli){
	try{
	   $data = array();   
	 
	   $idaccion    = $mysqli->real_escape_string( isset( $_POST['contacto']['idaccion'] )     ? $_POST['contacto']['idaccion'] : '');
	   $descontacto = $mysqli->real_escape_string( isset( $_POST['contacto']['descontacto'] ) ? $_POST['contacto']['descontacto'] : '');
	   $desfuente   = $mysqli->real_escape_string( isset( $_POST['contacto']['desfuente'] )   ? $_POST['contacto']['desfuente'] : '');
	   $telefono    = $mysqli->real_escape_string( isset( $_POST['contacto']['telefono'] )    ? $_POST['contacto']['telefono'] : '');
	   $email       = $mysqli->real_escape_string( isset( $_POST['contacto']['email'] )       ? $_POST['contacto']['email'] : '');
	   $verificado  = $mysqli->real_escape_string( isset( $_POST['contacto']['verificado'] )  ? $_POST['contacto']['verificado'] : '');

	   $user = $mysqli->real_escape_string( isset( $_POST['user'] ) ? $_POST['user'] : '');
	   $idcontacto = $mysqli->real_escape_string( isset( $_POST['contacto']['idcontacto'] ) ? $_POST['contacto']['idcontacto'] : '');

	   if(empty($idcontacto)){
		  $query = "INSERT INTO contactos (desfuente, descontacto, telefono, email, verificado) VALUES ('$desfuente', '$descontacto', '$telefono', '$email', '$verificado')";
	   }else{
		  $query = "UPDATE contactos SET desfuente = '$desfuente', descontacto = '$descontacto', telefono = '$telefono', email = '$email', verificado = '$verificado' WHERE idcontacto = $idcontacto";  
	   }
	   error_log($query);
	   if( $mysqli->query( $query ) ){
		  $data['success'] = true;
		  if($idcontacto!= NULL ||  $idcontacto != '' ) {
			 $data['message'] = 'Contacto  actualizado exitosamente.';
			 $data['idcontacto'] = (int)  $idcontacto;
			  $idcontacto = (int)  $idcontacto;
		  } else {
			 $data['message'] = 'Contacto insertado exitosamente.';
			 $data['idcontacto'] = (int) $mysqli->insert_id;
			 $idcontacto = (int) $mysqli->insert_id;
		  }
		   
	   }else{
		  throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
	   }
		    
		  $mysqli->close();
		  echo json_encode($data);
	   
	   
	   exit;
	}catch (Exception $e){
	   $data = array();
	   $data['success'] = false;
	   $data['message'] = $e->getMessage();
	   echo json_encode($data);
	   exit;
	}
 }




 function saveusuario($mysqli){
	try{
	   $data = array();   
	 
	   $descontacto    = $mysqli->real_escape_string( isset( $_POST['usuario']['descontacto'] )     ? $_POST['usuario']['descontacto'] : '');
	   $fuente         = $mysqli->real_escape_string( isset( $_POST['usuario']['fuente'] )          ? $_POST['usuario']['fuente'] : '');
	   $usuario        = $mysqli->real_escape_string( isset( $_POST['usuario']['usuario'] )         ? $_POST['usuario']['usuario'] : '');
	   $idcontacto     = $mysqli->real_escape_string( isset( $_POST['usuario']['idcontacto'] )      ? $_POST['usuario']['idcontacto'] : '');

	   $query = "UPDATE contactos SET desfuente = '$fuente', descontacto = '$descontacto', user = '$usuario' WHERE idcontacto = $idcontacto";  
	   
	   error_log($query);
	   if( $mysqli->query( $query ) ){
		  $data['success'] = true;
		  if($idcontacto!= NULL ||  $idcontacto != '' ) {
			 $data['message'] = 'Operacion realizada exitosamente.';
			 $data['idcontacto'] = (int)  $idcontacto;
			  $idcontacto = (int)  $idcontacto;
		  } else {
			 $data['message'] = 'Operacion realizada exitosamente.';
			 $data['idcontacto'] = (int) $mysqli->insert_id;
			 $idcontacto = (int) $mysqli->insert_id;
		  }
		   
	   }else{
		  throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
	   }
		    
		  $mysqli->close();
		  echo json_encode($data);
	   
	   
	   exit;
	}catch (Exception $e){
	   $data = array();
	   $data['success'] = false;
	   $data['message'] = $e->getMessage();
	   echo json_encode($data);
	   exit;
	}
 }




function getcontactos($mysqli){
	try{
		
		$admin  = $mysqli->real_escape_string(isset( $_POST['user']['admin'] ) ? $_POST['user']['admin'] : '');
        $user  = $mysqli->real_escape_string(isset( $_POST['user']['user'] ) ? $_POST['user']['user'] : '');

		if($admin == 'SI'){
		   $query = "SELECT * FROM contactos";
		}
		else{
			$query = "SELECT * FROM contactos WHERE user = '$user'";
		}
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontacto'] = (int) $row['idcontacto'];
			$row['verificado'] = (int) $row['verificado'] == 1 ? true : false;
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function gethistorial($mysqli){
	
	 $descontacto  = $mysqli->real_escape_string(isset( $_POST['descontacto']['descontacto'] ) ? $_POST['descontacto']['descontacto'] : '');

	 try{
		 $query = "SELECT		         a.*,
									     u.shortname,
									     e.desestatus
					 from 			     accionesrealizadas a
					 left outer join  	 usuarios u
					 on 			     u.user = a.user
					 left outer join     estatus e
					 on 			     e.idestatus = a.idestatus
					 where 			     a.descontacto = '$descontacto'
					 order by 		     a.fechaaccion desc";
		 $result = $mysqli->query( $query );
		 $data = array();
		 while ($row = $result->fetch_assoc()) {
			$row['idstatus'] = (int) $row['idestatus'];
			 $data['data'][] = $row;
		 }
		 $data['success'] = true;
 
		 echo json_encode($data);
		 exit;
	 
	 }catch (Exception $e){
		 $data = array();
		 $data['success'] = false;
		 $data['message'] = $e->getMessage();
		 echo json_encode($data);
		 exit;
	 }
 }

 function getestatus($mysqli){
	try{
	
		$query = "SELECT * FROM estatus order by idestatus asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idestatus'] = (int) $row['idestatus'];
			$row['checked'] = true;
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function getagenda($mysqli){
	$user = $mysqli->real_escape_string(isset( $_POST['user'] ) ? $_POST['user'] : '');
	$fecha = $mysqli->real_escape_string(isset( $_POST['fecha'] ) ? $_POST['fecha'] : '');

	try{
	
		$query = "  SELECT * FROM accionesrealizadas
					WHERE 		  accionesrealizadas.user = '$user'
					and 		  DATE_FORMAT(accionesrealizadas.fechasiguientecontacto,'%Y-%m-%d') = '$fecha'
					order by 	  fechasiguientecontacto asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idestatus'] = (int) $row['idestatus'];
			
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function gethistorialdia($mysqli){
	$user = $mysqli->real_escape_string(isset( $_POST['user'] ) ? $_POST['user'] : '');
	$fecha = $mysqli->real_escape_string(isset( $_POST['fecha'] ) ? $_POST['fecha'] : '');

	try{
	
		$query = "SELECT			a.*,
									u.name,
					            	e.desestatus
					from 			accionesrealizadas a
					inner join  	usuarios u
					on 				u.user = a.user
					left outer join estatus e
					on 				e.idestatus = a.idestatus
					where 			a.user = '$user'
					and 			DATE_FORMAT(a.fechaaccion,'%Y-%m-%d') = '$fecha'
					order by 		a.fechaaccion desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idaccion'] = (int) $row['idaccion'];
			$row['idrequerimiento'] = (int) $row['idrequerimiento'];
			$row['idestatus'] = (int) $row['idestatus'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getusuarios($mysqli){
	try{
	
		$query = "SELECT * FROM usuarios";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function getaccionesdiarias($mysqli){
	try{
		$fecha = $mysqli->real_escape_string( isset( $_POST['fecha'] ) ? $_POST['fecha'] : '');
		$user = $mysqli->real_escape_string( isset( $_POST['user'] ) ? $_POST['user'] : '');

		$query = "SELECT 'Llamada' as accion,count(*) as total 	from accionesrealizadas WHERE  tipoaccion = 'Llamada' and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Correo',count(*) 						from accionesrealizadas WHERE  tipoaccion = 'Envío de Correo'	and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Cita',count(*) 						from accionesrealizadas WHERE  tipoaccion = 'Cita Agendada'	and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Visita',count(*) 						from accionesrealizadas WHERE  tipoaccion = 'Visita'			and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Otra',count(*) 						from accionesrealizadas WHERE  tipoaccion = 'Otra'				and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Contacto Efectivo',count(*) 			from accionesrealizadas WHERE  user = '$user' and tipoaccion = 'Contacto Efectivo'	and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'";

		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['total'] = (int) $row['total'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

