<?php

// respond to preflights
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	// return only the headers and not the content
	// only allow CORS if we're doing a GET - i.e. no saving for now.
	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']) &&
		$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] == 'GET' &&
		isset($_SERVER['HTTP_ORIGIN']) &&
		is_approved($_SERVER['HTTP_ORIGIN'])) {
	  $allowedOrigin = $_SERVER['HTTP_ORIGIN'];
	  $allowedHeaders = get_allowed_headers($allowedOrigin);
	  header('Access-Control-Allow-Methods: GET, POST, OPTIONS'); //...
	  header('Access-Control-Allow-Origin: ' . $allowedOrigin);
	  header('Access-Control-Allow-Headers: ' . $allowedHeaders);
	  header('Access-Control-Max-Age: 3600');
	}
	exit;
  }
  
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, X-Auth-Token');

/*
Site : http:www.smarttutorials.net
Author :muni
*/



//site specific configuration declartion

define('BASE_PATH', 'http://localhost/darhe/');
define('DB_HOST', 'localhost');
define('DB_NAME', 'darhe');
define('DB_USERNAME','root');
define('DB_PASSWORD','');

/*
define('BASE_PATH', 'http://darhe.com.mx/sistema/v3/');
define('DB_HOST', 'mariadb-157.wc2.phx1.stabletransit.com');
define('DB_NAME', '1026083_darhe_v3');
define('DB_USERNAME','1026083_darhe_v3');
define('DB_PASSWORD','123456Da');
*/

$mysqli  = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
if (mysqli_connect_errno()) {
	echo("Failed to connect, the error message is : ". mysqli_connect_error());
	exit();
}
mysqli_set_charset($mysqli, 'utf8');