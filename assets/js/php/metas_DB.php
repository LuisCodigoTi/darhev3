<?php
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
      case "getmetas":
	        getmetas($mysqli);
            break;
      case "save_metas":
	        save_metas($mysqli);
			break;
	  default:
	     invalidRequest();
	}
}else{
	invalidRequest();
}


function getmetas($mysqli){
    try{
       $query = "SELECT              u.user,
                                     u.name,
                                     m.monto,
                                     m.llamadas,
                                     m.correos,
                                     m.visitas,
                                     m.citas
                   FROM              usuarios u
                   LEFT OUTER JOIN   metasxusuario m
                   ON                m.user = u.user";
       $result = $mysqli->query( $query );
       $data = array();
       while ($row = $result->fetch_assoc()) {
          $row['monto'] = (float) $row['monto'];
          $row['llamadas'] = (int) $row['llamadas'];
          $row['correos'] = (int) $row['correos'];
          $row['visitas'] = (int) $row['visitas'];
          $row['citas'] = (int) $row['citas'];
          $data['data'][] = $row;
       }
       $data['success'] = true;
 
       echo json_encode($data);
       exit;
    
    }catch (Exception $e){
       $data = array();
       $data['success'] = false;
       $data['message'] = $e->getMessage();
       echo json_encode($data);
       exit;
    }
 }

 function save_metas($mysqli){
    $data = array();
 
    try{
       $user     = $mysqli->real_escape_string(isset( $_POST['metas']['name'] ) ? $_POST['metas']['name'] : '');
       $monto    = $mysqli->real_escape_string(isset( $_POST['metas']['monto'] ) ? $_POST['metas']['monto'] : '');
       $llamadas = $mysqli->real_escape_string(isset( $_POST['metas']['llamadas'] ) ? $_POST['metas']['llamadas'] : '');
       $correos  = $mysqli->real_escape_string(isset( $_POST['metas']['correos'] ) ? $_POST['metas']['correos'] : '');
       $visitas  = $mysqli->real_escape_string(isset( $_POST['metas']['visitas'] ) ? $_POST['metas']['visitas'] : '');
       $citas    = $mysqli->real_escape_string(isset( $_POST['metas']['citas'] ) ? $_POST['metas']['citas'] : '');
 
       if($user == '' || $monto == '' || $llamadas == '' || $correos == '' || $visitas == '' || $citas == ''){
          throw new Exception( "Campos requeridos faltantes" );
       }
       
 
       $query = "DELETE FROM metasxusuario where user = '$user'; ";
       $query = $query .
                "INSERT INTO metasxusuario(user,monto,llamadas,correos,visitas,citas) VALUES('$user',$monto,$llamadas,$correos,$visitas,$citas); ";
 
       if( $mysqli->multi_query( $query ) ){
          $data['success'] = true;
          $data['message'] = 'Metas registradas exitosamente.';
          $data['idaccion'] = (int) $mysqli->insert_id;
       }else{
          throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
       }
       $mysqli->close();
       echo json_encode($data);
       exit;
    }catch (Exception $e){
       $data['success'] = false;
       $data['message'] = $e->getMessage();
       echo json_encode($data);
       exit;
    }
 }


function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

