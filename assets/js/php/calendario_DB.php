<?php
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {

      case "getagenda_calendario":
            getagenda_calendario($mysqli);
            break;
      case "getagenda_calendarioyear":
            getagenda_calendarioyear($mysqli);
            break;
      case "gethistorialdia_d1":  
            gethistorialdia_d1($mysqli);
            break;
      case "getaccionesdiariasxusuario_d1":  
            getaccionesdiariasxusuario_d1($mysqli);
			break;
	  default:
	     invalidRequest();
	}
}else{
	invalidRequest();
}

function getagenda_calendario($mysqli){
	
	$fecha = $mysqli->real_escape_string(isset( $_POST['fecha'] ) ? $_POST['fecha'] : '');
    $admin  = $mysqli->real_escape_string(isset( $_POST['user']['admin'] ) ? $_POST['user']['admin'] : '');
    $user  = $mysqli->real_escape_string(isset( $_POST['user']['user'] ) ? $_POST['user']['user'] : '');

	try{
	 if($admin == 'SI'){
		$query = "SELECT		distinct contactos.*,
								estatus.desestatus,
					            accionesrealizadas.fechasiguientecontacto
					FROM 		contactos,
								usuarios,
								estatus,
					            accionesrealizadas
					where 		estatus.idestatus=contactos.idestatus
					and   		accionesrealizadas.descontacto = contactos.descontacto
					and 		DATE_FORMAT(accionesrealizadas.fechasiguientecontacto,'%Y-%m-%d') = '$fecha'
					order by 	fechasiguientecontacto";
	 }else{
        $query = "SELECT		distinct contactos.*,
								estatus.desestatus,
					            accionesrealizadas.fechasiguientecontacto
					FROM 		contactos,
								usuarios,
								estatus,
					            accionesrealizadas
					where 		estatus.idestatus=contactos.idestatus
					and   		accionesrealizadas.descontacto = contactos.descontacto
					and         accionesrealizadas.user = '$user' 
					and 		DATE_FORMAT(accionesrealizadas.fechasiguientecontacto,'%Y-%m-%d') = '$fecha'
					order by 	fechasiguientecontacto";
	 }

		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
		
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getagenda_calendarioyear($mysqli){
	
	$admin  = $mysqli->real_escape_string(isset( $_POST['user']['admin'] ) ? $_POST['user']['admin'] : '');
	$user  = $mysqli->real_escape_string(isset( $_POST['user']['user'] ) ? $_POST['user']['user'] : '');
	
 
     try{
	 
		if($admin == 'SI'){
			$query = "SELECT 'Contacto' as tipo_evento,
			                  contactos.descontacto,
			                  accionesrealizadas.fechasiguientecontacto as fecha,
			                  accionesrealizadas.descontacto,
			                  accionesrealizadas.estatusdesglosado,
			                  usuarios.name
                        FROM  contactos,
			                  accionesrealizadas,
			                  usuarios
                       where  accionesrealizadas.descontacto = contactos.descontacto
                         and  accionesrealizadas.fechasiguientecontacto is not null
                         and  usuarios.user = accionesrealizadas.user
                       
					   UNION
                      SELECT 'Cita' as tipo_evento,
			                  contactos.descontacto,
			                  accionesrealizadas.fechacita,
			                  accionesrealizadas.descontacto,
			                  accionesrealizadas.estatusdesglosado,
		                      usuarios.name
                        FROM  contactos,
			                  accionesrealizadas,
			                  usuarios
                       where  accionesrealizadas.descontacto = contactos.descontacto
                         and  accionesrealizadas.fechacita is not null
                         and  usuarios.user = accionesrealizadas.user";
		}else{
         $query = "SELECT		'Contacto' as tipo_evento,
                                 contactos.descontacto,
                                 accionesrealizadas.fechasiguientecontacto as fecha,
                                 accionesrealizadas.descontacto,
                                 accionesrealizadas.estatusdesglosado,
                                 usuarios.name
                     FROM 		 contactos,
                                 accionesrealizadas,
                                 usuarios
                     where 		 accionesrealizadas.descontacto = contactos.descontacto
                     and 		 accionesrealizadas.fechasiguientecontacto is not null
                     and 		 usuarios.user = accionesrealizadas.user
					 and         accionesrealizadas.user = '$user'
                   UNION
                   SELECT		'Cita' as tipo_evento,
                                 contactos.descontacto,
                                 accionesrealizadas.fechacita,
                                 accionesrealizadas.descontacto,
                                 accionesrealizadas.estatusdesglosado,
                                 usuarios.name
                     FROM 		 contactos,
                                 accionesrealizadas,
                                 usuarios
                     where 		accionesrealizadas.descontacto = contactos.descontacto
                     and 		accionesrealizadas.fechacita is not null
                     and 		usuarios.user = accionesrealizadas.user
					 and        accionesrealizadas.user = '$user'";
		}

         $result = $mysqli->query( $query );
         $data = array();
         while ($row = $result->fetch_assoc()) {
             $data['data'][] = $row;
         }
         $data['success'] = true;
 
         echo json_encode($data);
         exit;
     
     }catch (Exception $e){
         $data = array();
         $data['success'] = false;
         $data['message'] = $e->getMessage();
         echo json_encode($data);
         exit;
     }
 }


 function gethistorialdia_d1($mysqli){
	$fecha = $mysqli->real_escape_string(isset( $_POST['fecha'] ) ? $_POST['fecha'] : '');

	try{
	
		$query = "SELECT			    a.*,
									    u.shortname,
					            	    e.desestatus,
					            	    c.descontacto
					from 		    	accionesrealizadas a
					left outer join  	usuarios u
					on 				    u.user = a.user
					left outer join 	contactos c
					on 				    c.descontacto = a.descontacto
					left outer join     estatus e
					on 				    e.idestatus = a.idestatus
					where 			    DATE_FORMAT(a.fechaaccion,'%Y-%m-%d') = '$fecha'
					order by 		    a.fechaaccion desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idaccion'] = (int) $row['idaccion'];
			$row['idestatus'] = (int) $row['idestatus'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getaccionesdiariasxusuario_d1($mysqli){
	try{
        $fecha = $mysqli->real_escape_string( isset( $_POST['fecha'] ) ? $_POST['fecha'] : '');
        $user = $mysqli->real_escape_string( isset( $_POST['user'] )  ? $_POST['user'] : '');

		$query = "SELECT 'Llamada' as accion,count(*) as total 	from accionesrealizadas WHERE tipoaccion = 'Llamada' 		 and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Correo',count(*) 						from accionesrealizadas WHERE tipoaccion = 'Envío de Correo' and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Cita',count(*) 						from accionesrealizadas WHERE tipoaccion = 'Cita Agendada'	 and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Visita',count(*) 						from accionesrealizadas WHERE tipoaccion = 'Visita'			 and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Otra',count(*) 						from accionesrealizadas WHERE tipoaccion = 'Otra'			 and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Contacto Efectivo',count(*) 			from accionesrealizadas WHERE user = '$user'                 and tipoaccion = 'Contacto Efectivo'	and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'";

		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['total'] = (int) $row['total'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}