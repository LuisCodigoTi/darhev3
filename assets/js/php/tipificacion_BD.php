<?php
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
	  case "getusuarios":
            getusuarios($mysqli);
            break;
      case "getcontactostipif2":
            getcontactostipif2($mysqli);
            break;
      case "getcontactostipif3":
            getcontactostipif3($mysqli);
            break;
	  default:
	     invalidRequest();
	}
}else{
	invalidRequest();
}


function getusuarios($mysqli){
	try{
	   ini_set('memory_limit', '-1');
	
	   $query = "SELECT 		'TODOS' user,
								'TODOS' name
					union            
					select		user,
								name
					from 		usuarios 
					where 		invpros = 'SI' 
					or 			admin = 'SI' 
					or 			tablerocontrol = 'SI'";
	   $result = $mysqli->query( $query );
	   $data = array();
	   while ($row = $result->fetch_assoc()) {
		  $data['data'][] = $row;
	   }
	   $data['success'] = true;
 
	   echo json_encode($data);
	   exit;
	
	}catch (Exception $e){
	   $data = array();
	   $data['success'] = false;
	   $data['message'] = $e->getMessage();
	   echo json_encode($data);
	   exit;
	}
 }

 
 function getcontactostipif2($mysqli){
	try{
	   ini_set('memory_limit', '-1');
 
	   $query = "SELECT			estatus.desestatus, 
								contactos.desfuente,
								count(*) total
				from 			contactos
				left outer join estatus
				on 				estatus.idestatus = contactos.idestatus
                where 			contactos.idestatus<>0
				group by		estatus.desestatus, contactos.desfuente
                union
				SELECT			'TOTAL REGISTROS TOCADOS', 
                                contactos.desfuente,
								count(*) total
				from 			contactos
                where 			contactos.idestatus<>0
				group by		contactos.desfuente
                union
				SELECT			'TOTAL REGISTROS BASE DE DATOS', 
                                contactos.desfuente,
								count(*) total
				from 			contactos
				group by		contactos.desfuente
				order by 		2,1";
 
	   $result = $mysqli->query( $query );
	   $data = array();
	   while ($row = $result->fetch_assoc()) {
		  $row['total'] = (int) $row['total'];
		  $data['data'][] = $row;
	   }
	   $data['success'] = true;
 
	   echo json_encode($data);
	   exit;
	
	}catch (Exception $e){
	   $data = array();
	   $data['success'] = false;
	   $data['message'] = $e->getMessage();
	   echo json_encode($data);
	   exit;
	}
 }


  function getcontactostipif3($mysqli){
	try{
	   ini_set('memory_limit', '-1');

	   $user = $mysqli->real_escape_string(isset( $_POST['user'] ) ? $_POST['user'] : '');

	   $query = "SELECT			estatus.desestatus, 
								contactos.desfuente,
								count(*) total
				from 			contactos
				inner join 		estatus
				on 				estatus.idestatus = contactos.idestatus
				inner join 		usuarios
				on 				usuarios.user = '$user'
				where 			contactos.idestatus<>0
				group by		estatus.desestatus, contactos.desfuente
				union
				SELECT			'TOTAL REGISTROS TOCADOS', 
                                contactos.desfuente,
								count(*) total
				from 			contactos
				inner join 		usuarios
				on 				usuarios.user = '$user'
				where 			contactos.idestatus<>0
				group by		contactos.desfuente
				union
				SELECT			'TOTAL REGISTROS BASE DE DATOS', 
                               contactos.desfuente,
								count(*) total
				from 			contactos
				group by		contactos.desfuente
				order by 		2,1";
 error_log($query);
	   $result = $mysqli->query( $query );
	   $data = array();
	   while ($row = $result->fetch_assoc()) {
		  $row['total'] = (int) $row['total'];
		  $data['data'][] = $row;
	   }
	   $data['success'] = true;
 
	   echo json_encode($data);
	   exit;
	
	}catch (Exception $e){
	   $data = array();
	   $data['success'] = false;
	   $data['message'] = $e->getMessage();
	   echo json_encode($data);
	   exit;
	}
 }
function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}