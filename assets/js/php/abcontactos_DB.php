<?php
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
      case "getcontactos":
            getcontactos($mysqli);
			break;
      case "savecontacto":
            savecontacto($mysqli);
			break;
	  default:
	     invalidRequest();
	}
}else{
	invalidRequest();
}

function getcontactos($mysqli){
	try{
		
		$admin  = $mysqli->real_escape_string(isset( $_POST['user']['admin'] ) ? $_POST['user']['admin'] : '');
        $user  = $mysqli->real_escape_string(isset( $_POST['user']['user'] ) ? $_POST['user']['user'] : '');

		if($admin == 'SI'){
		   $query = "SELECT * FROM contactos";
		}
		else{
			$query = "SELECT * FROM contactos WHERE user = '$user'";
		}
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontacto'] = (int) $row['idcontacto'];
			$row['verificado'] = (int) $row['verificado'] == 1 ? true : false;
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function savecontacto($mysqli){
	try{
	   $data = array();   
	   $descontacto = $mysqli->real_escape_string( isset( $_POST['contacto']['descontacto'] ) ? $_POST['contacto']['descontacto'] : '');
	   $desfuente   = $mysqli->real_escape_string( isset( $_POST['contacto']['desfuente'] )   ? $_POST['contacto']['desfuente'] : '');
	   $telefono    = $mysqli->real_escape_string( isset( $_POST['contacto']['telefono'] )    ? $_POST['contacto']['telefono'] : '');
	   $email       = $mysqli->real_escape_string( isset( $_POST['contacto']['email'] )       ? $_POST['contacto']['email'] : '');

	   $user = $mysqli->real_escape_string( isset( $_POST['user'] ) ? $_POST['user'] : '');
	   $idcontacto = $mysqli->real_escape_string( isset( $_POST['contacto']['idcontacto'] ) ? $_POST['contacto']['idcontacto'] : '');

	   if(empty($idcontacto)){
		  $query = "INSERT INTO contactos (desfuente, descontacto, telefono, email, verificado, idestatus, fecsigcont, user) VALUES ('$desfuente', '$descontacto', '$telefono', '$email', '', '', '', '')";
	   }else{
		  $query = "UPDATE contactos SET desfuente = '$desfuente', descontacto = '$descontacto', telefono = '$telefono', email = '$email' WHERE idcontacto = $idcontacto";  
	   }
	   error_log($query);
	   if( $mysqli->query( $query ) ){
		  $data['success'] = true;
		  if($idcontacto!= NULL ||  $idcontacto != '' ) {
			 $data['message'] = 'Contacto  actualizado exitosamente.';
			 $data['idcontacto'] = (int)  $idcontacto;
			  $idcontacto = (int)  $idcontacto;
		  } else {
			 $data['message'] = 'Contacto insertado exitosamente.';
			 $data['idcontacto'] = (int) $mysqli->insert_id;
			 $idcontacto = (int) $mysqli->insert_id;
		  }
		   
	   }else{
		  throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
	   }
		    
		  $mysqli->close();
		  echo json_encode($data);
	   
	   
	   exit;
	}catch (Exception $e){
	   $data = array();
	   $data['success'] = false;
	   $data['message'] = $e->getMessage();
	   echo json_encode($data);
	   exit;
	}
 }

function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}
