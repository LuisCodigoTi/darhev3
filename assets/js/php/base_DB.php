<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "getempresas":
			getempresas($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

function getempresas($mysqli){
	try{
	
		ini_set('memory_limit', '-1');
		$query = "SELECT		empresas_bitacora.*,
					            usuarios.name
					FROM 		empresas_bitacora,usuarios
					where 		usuarios.user COLLATE latin1_general_ci = empresas_bitacora.user COLLATE latin1_general_ci
					order by 	desempresa asc, horfecact asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idempresa'] = (int) $row['idempresa'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}
