<?php
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
	  case "getmetas2":
	        getmetas2($mysqli);
	  default:
	     invalidRequest();
	}
}else{
	invalidRequest();
}

function getmetas2($mysqli){
   try{
      ini_set('memory_limit', '-1');
   
      $query = "SELECT  user,
                        name,
                        shortname,
                        IFNULL((select monto              from metasxusuario m      where m.user = u.user),0) meta_m,
                        IFNULL((select sum(montocotizado) from accionesrealizadas m where m.user = u.user and MONTH(fechaaccion) = MONTH(NOW()) and YEAR(fechaaccion) = YEAR(NOW())),0) as real_m,
                        IFNULL((select llamadas           from metasxusuario m      where m.user = u.user),0) meta_ll,
                        IFNULL((select count(*)           from accionesrealizadas m where m.user = u.user and MONTH(fechaaccion) = MONTH(NOW()) and YEAR(fechaaccion) = YEAR(NOW()) and tipoaccion='Contacto Efectivo'),0) as real_ll,
                        IFNULL((select citas              from metasxusuario m      where m.user = u.user),0) meta_ci,
                        IFNULL((select count(*)           from accionesrealizadas m where m.user = u.user and MONTH(fechaaccion) = MONTH(NOW()) and YEAR(fechaaccion) = YEAR(NOW()) and tipoaccion='Cita Agendada'),0) as real_ci,
						IFNULL((select count(*)           from accionesrealizadas m where m.user = u.user and date(fechaaccion) = CURDATE() and tipoaccion='Llamada'),0) as diar_ll,
						IFNULL((select count(*)           from accionesrealizadas m where m.user = u.user and date(fechaaccion) = CURDATE() and tipoaccion='Contacto Efectivo'),0) as diar_ce
                  from  usuarios u";

      $result = $mysqli->query( $query );
      $data = array();
      while ($row = $result->fetch_assoc()) {
         $row['meta_m'] = (float) $row['meta_m'];
         $row['real_m'] = (float) $row['real_m'];
         $row['meta_ll'] = (int) $row['meta_ll'];
         $row['real_ll'] = (int) $row['real_ll'];
         $row['meta_ci'] = (int) $row['meta_ci'];
         $row['real_ci'] = (int) $row['real_ci'];
         $row['diar_ll'] = (int) $row['diar_ll'];
         $row['diar_ce'] = (int) $row['diar_ce'];
         if($row['meta_m'] == 0 &&
            $row['real_m'] == 0 &&
            $row['meta_ll'] == 0 &&
            $row['real_ll'] == 0 &&
            $row['meta_ci'] == 0 &&
            $row['real_ci'] == 0) {}
         else {
			if($row['meta_ll'] > 0) $row['efec_ll'] = $row['real_ll'] * 100 / $row['meta_ll']; else $row['efec_ll'] = 0;
			if($row['meta_ci'] > 0) $row['efec_ci'] = $row['real_ci'] * 100 / $row['meta_ci']; else $row['efec_ci'] = 0;
			if($row['meta_m'] > 0)  $row['efec_m']  = $row['real_m']  * 100 / $row['meta_m'];  else $row['efec_m'] = 0;
			$data['data'][] = $row;
		 }
      }
      $data['success'] = true;

      echo json_encode($data);
      exit;
   
   }catch (Exception $e){
      $data = array();
      $data['success'] = false;
      $data['message'] = $e->getMessage();
      echo json_encode($data);
      exit;
   }
}


function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}