<?php
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
	  case "getcontactos":
            getcontactos($mysqli);
			break;
	  case "gethistorial":
	        gethistorial($mysqli);
			break;	 
	  case "getestatus":
	        getestatus($mysqli);
			break;	 
	  case "getagenda":
	        getagenda($mysqli);
			break;	 
	  case "gethistorialdia":
			gethistorialdia($mysqli);
	  case "getaccionesdiarias":
			getaccionesdiarias($mysqli);
	  case "getusuarios":
	        getusuarios($mysqli);
	  default:
	     invalidRequest();
	}
}else{
	invalidRequest();
}

function getcontactos($mysqli){
	try{
		
		$query = "SELECT * FROM contactos";
		
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontacto'] = (int) $row['idcontacto'];
			$row['verificado'] = (int) $row['verificado'] == 1 ? true : false;
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function gethistorial($mysqli){
	
	 $descontacto  = $mysqli->real_escape_string(isset( $_POST['descontacto']['descontacto'] ) ? $_POST['descontacto']['descontacto'] : '');

	 try{
		 $query = "SELECT		         a.*,
									     u.shortname,
									     e.desestatus
					 from 			     accionesrealizadas a
					 left outer join  	 usuarios u
					 on 			     u.user = a.user
					 left outer join     estatus e
					 on 			     e.idestatus = a.idestatus
					 where 			     a.descontacto = '$descontacto'
					 order by 		     a.fechaaccion desc";
		 $result = $mysqli->query( $query );
		 $data = array();
		 while ($row = $result->fetch_assoc()) {
			$row['idstatus'] = (int) $row['idestatus'];
			 $data['data'][] = $row;
		 }
		 $data['success'] = true;
 
		 echo json_encode($data);
		 exit;
	 
	 }catch (Exception $e){
		 $data = array();
		 $data['success'] = false;
		 $data['message'] = $e->getMessage();
		 echo json_encode($data);
		 exit;
	 }
 }

 function getestatus($mysqli){
	try{
	
		$query = "SELECT * FROM estatus order by idestatus asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idestatus'] = (int) $row['idestatus'];
			$row['checked'] = true;
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function getagenda($mysqli){
	$user = $mysqli->real_escape_string(isset( $_POST['user'] ) ? $_POST['user'] : '');
	$fecha = $mysqli->real_escape_string(isset( $_POST['fecha'] ) ? $_POST['fecha'] : '');

	try{
	
		$query = "  SELECT * FROM accionesrealizadas
					WHERE 		  DATE_FORMAT(accionesrealizadas.fechasiguientecontacto,'%Y-%m-%d') = '$fecha'
					order by 	  fechasiguientecontacto asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idestatus'] = (int) $row['idestatus'];
			
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function gethistorialdia($mysqli){
	$user = $mysqli->real_escape_string(isset( $_POST['user'] ) ? $_POST['user'] : '');
	$fecha = $mysqli->real_escape_string(isset( $_POST['fecha'] ) ? $_POST['fecha'] : '');

	try{
	
		$query = "SELECT			a.*,
									u.name,
					            	e.desestatus
					from 			accionesrealizadas a
					inner join  	usuarios u
					on 				u.user = a.user
					left outer join estatus e
					on 				e.idestatus = a.idestatus
					where 			DATE_FORMAT(a.fechaaccion,'%Y-%m-%d') = '$fecha'
					order by 		a.fechaaccion desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idaccion'] = (int) $row['idaccion'];
			$row['idrequerimiento'] = (int) $row['idrequerimiento'];
			$row['idestatus'] = (int) $row['idestatus'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getusuarios($mysqli){
	try{
	
		$query = "SELECT * FROM usuarios";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function getaccionesdiarias($mysqli){
	try{
		$fecha = $mysqli->real_escape_string( isset( $_POST['fecha'] ) ? $_POST['fecha'] : '');
		$user = $mysqli->real_escape_string( isset( $_POST['user'] ) ? $_POST['user'] : '');

		$query = "SELECT 'Llamada' as accion,count(*) as total 	from accionesrealizadas WHERE  tipoaccion = 'Llamada' and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Correo',count(*) 						from accionesrealizadas WHERE  tipoaccion = 'Envío de Correo'	and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Cita',count(*) 						from accionesrealizadas WHERE  tipoaccion = 'Cita Agendada'	and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Visita',count(*) 						from accionesrealizadas WHERE  tipoaccion = 'Visita'			and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Otra',count(*) 						from accionesrealizadas WHERE  tipoaccion = 'Otra'				and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Contacto Efectivo',count(*) 			from accionesrealizadas WHERE  user = '$user' and tipoaccion = 'Contacto Efectivo'	and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'";

		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['total'] = (int) $row['total'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

