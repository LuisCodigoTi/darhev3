'use strict';

/**
 * Config for the router
 */
app.config(['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'JS_REQUIRES',
function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, jsRequires) {

    app.controller = $controllerProvider.register;
    app.directive = $compileProvider.directive;
    app.filter = $filterProvider.register;
    app.factory = $provide.factory;
    app.service = $provide.service;
    app.constant = $provide.constant;
    app.value = $provide.value;

    // LAZY MODULES

    $ocLazyLoadProvider.config({
        debug: false,
        events: true,
        modules: jsRequires.modules
    });

    // APPLICATION ROUTES
    // -----------------------------------
    // For any unmatched url, redirect to /app/dashboard
    $urlRouterProvider.otherwise("/login/signin");
    //
    // Set up the states
    $stateProvider.state('app', {
        url: "/app",
        templateUrl: "assets/views/app.html",
        resolve: loadSequence('modernizr', 'moment', 'angularMoment', 'uiSwitch', 'perfect-scrollbar-plugin', 'toaster', 'ngAside', 'vAccordion', 'sweet-alert', 'chartjs', 'tc.chartjs', 'oitozero.ngSweetAlert', 'login'),
        abstract: true
    }).state('app.inicio', {
        url: "/inicio",
        templateUrl: "assets/views/inicio.html",
        resolve: loadSequence('jquery-sparkline'),
        title: 'Inicio',
        ncyBreadcrumb: {
            label: 'Inicio'
        }
    }).state('app.metas', {
        url: "/metas",
        templateUrl: "assets/views/metas.php",
        resolve: loadSequence('ngTable', 'metas'),
        title: 'Metas',
        ncyBreadcrumb: {
            label: 'Metas'
        }
    }).state('app.prospeccion', {
        url: '/prospeccion',
        templateUrl: "assets/views/prospeccion.php",
        title: 'Prospección',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'Prospección'
        },
        resolve: loadSequence('ui.select','ngTable', 'darhe')
    }).state('app.calendario', {
        url: '/Calendario/:tipo',
        templateUrl: "assets/views/calendario.php",
        title: 'Calendario',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'Calendario'
        },
        resolve: loadSequence('ngTable', 'calendario')
    }).state('app.base', {
        url: '/base-de-datos',
        template: '<div ui-view class="fade-in-up"></div>',
        title: 'Base de Datos',
        ncyBreadcrumb: {
            label: 'Base de Datos'
        }
    }).state('app.base.abcontactos', {
        url: '/abcontactos',
        templateUrl: "assets/views/abcontactos.php",
        title: 'ABC Contactos',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'ABC  Contactos'
        },
        resolve: loadSequence('ngTable', 'abcontactos')
    }).state('app.base.asigempresas', {
        url: '/asigempresas',
        templateUrl: "assets/views/asigempresas.php",
        title: 'Asignación de Empresas',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'Asignación de Empresas'
        },
        resolve: loadSequence('ngTable', 'darhe', 'ng-csv')
    }).state('app.base.exportmailing', {
        url: '/exportmailing',
        templateUrl: "assets/views/exportmailing.php",
        title: 'Exportar para Mailing',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'Exportar para Mailing'
        },
        resolve: loadSequence('ngTable', 'darhe', 'ng-csv')
    }).state('app.dashboard', {
        url: '/dashboard',
        template: '<div ui-view class="fade-in-up"></div>',
        title: 'Dashboard',
        ncyBreadcrumb: {
            label: 'Dashboard'
        }
    }).state('app.dashboard.dashboard1', {
        url: '/OperacionDiaria',
        templateUrl: "assets/views/dashboard1.php",
        title: 'Operación Diaria',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'Operación Diaria'
        },
        resolve: loadSequence('ngTable', 'operaciondiaria')
    }).state('app.dashboard.dashboard3', {
        url: '/TableroMetas',
        templateUrl: "assets/views/dashboard3.php",
        title: 'Metas',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'Metas'
        },
        resolve: loadSequence('ngTable', 'darhe')
    }).state('app.dashboard.metasmensual', {
        url: '/TableroMetasMensual',
        templateUrl: "assets/views/metasmensual.php",
        title: 'Metas',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'Metas'
        },
        resolve: loadSequence('ngTable', 'metasmensual')
    }).state('app.dashboard.dashboard5', {
        url: '/TotalesTipificacion',
        templateUrl: "assets/views/dashboard5.php",
        title: 'Totales por Tipificación',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'Totales por Tipificación'
        },
        resolve: loadSequence('ngTable', 'tipificacion')
    }).state('app.usuarios', {
        url: '/usuarios',
        templateUrl: "assets/views/usuarios.php",
        title: 'Usuarios',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'Usuarios'
        },
        resolve: loadSequence('ngTable', 'usuarios')
    })

	// Login routes

	.state('login', {
	    url: '/login',
	    template: '<div ui-view class="fade-in-right-big smooth"></div>',
	    abstract: true
	}).state('login.signin', {
	    url: '/signin',
	    templateUrl: "assets/views/login_login.html",
        resolve: loadSequence('login')
	}).state('login.forgot', {
	    url: '/forgot',
	    templateUrl: "assets/views/login_forgot.html"
	}).state('login.registration', {
	    url: '/registration',
	    templateUrl: "assets/views/login_registration.html"
	}).state('login.lockscreen', {
	    url: '/lock',
	    templateUrl: "assets/views/login_lock_screen.html"
	});

    // Generates a resolve object previously configured in constant.JS_REQUIRES (config.constant.js)
    function loadSequence() {
        var _args = arguments;
        return {
            deps: ['$ocLazyLoad', '$q',
			function ($ocLL, $q) {
			    var promise = $q.when(1);
			    for (var i = 0, len = _args.length; i < len; i++) {
			        promise = promiseThen(_args[i]);
			    }
			    return promise;

			    function promiseThen(_arg) {
			        if (typeof _arg == 'function')
			            return promise.then(_arg);
			        else
			            return promise.then(function () {
			                var nowLoad = requiredData(_arg);
			                if (!nowLoad)
			                    return $.error('Route resolve: Bad resource name [' + _arg + ']');
			                return $ocLL.load(nowLoad);
			            });
			    }

			    function requiredData(name) {
			        if (jsRequires.modules)
			            for (var m in jsRequires.modules)
			                if (jsRequires.modules[m].name && jsRequires.modules[m].name === name)
			                    return jsRequires.modules[m];
			        return jsRequires.scripts && jsRequires.scripts[name];
			    }
			}]
        };
    }
}]);