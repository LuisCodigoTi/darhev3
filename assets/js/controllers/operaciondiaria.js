'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('operaciondiaria', ["$rootScope", "$scope", "$filter", "NgTableParams", "$http",  "$modal", "$log", "SweetAlert", "$interval", "toaster", "uiCalendarConfig", '$timeout', '$compile', '$stateParams', function ($rootScope, $scope, $filter, NgTableParams, $http, $modal, $log, SweetAlert, $interval, toaster, uiCalendarConfig, $timeout, $compile, $stateParams) {
    
    var base_path = document.getElementById('base_path').value;
    var url = base_path + 'assets/js/php/operaciondiaria_DB.php';

    $scope.contactos = [];
    $scope.historial = [];
    $scope.guardausuario = [];
    $scope.datoscontactos = [];
    $scope.estatus = [];
    $scope.agenda = [];
    $scope.historiaHoy = [];
    $scope.accionesdiariasxusuario = [];
    $scope.usuarios = [];

    
    //*************************************************************************************************************************//
    //*************************************************************************************************************************//
    $scope.tableParamsHh = new NgTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            descontacto: 'asc' // initial sorting
        }
    }, {
        counts: [], // Hides page sizes
        total: $scope.historiaHoy.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.historiaHoy, params.filter()) : $scope.historiaHoy;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
    //*************************************************************************************************************************//
    //*************************************************************************************************************************//

    $scope.tableParamsA = new NgTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            descontacto: 'asc' // initial sorting
        }
    }, {
        counts: [], // Hides page sizes
        total: $scope.agenda.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.agenda, params.filter()) : $scope.agenda;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
    //*************************************************************************************************************************//
    //*************************************************************************************************************************//
    $scope.init = function () {
        console.log("LOGIN DE USUARIO");
        console.log($rootScope.user);
        $scope.waiting();
        $http({
            method: 'post',
            url: url,
            data: $.param({
                'type': 'getcontactos',
                'user': $rootScope.user
            }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).
        then(function (response) {
            if (response.data.success) {
                console.log(response.data.success);
                if (!angular.isUndefined(response.data.data)) {
                    $scope.contactos = response.data.data;
                    console.log($scope.contactos);
                } else {
                    $scope.contactos = [];
                    console.log($scope.contactos);
                }
                $scope.stopwaiting();
                //$scope.tableParamsC.reload();
            } else {
                $scope.contactos = [];
                //$scope.tableParamsC.reload();
                SweetAlert.swal({
                    title: "Error",
                    text: response.data.message,
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
            $http({
                method: 'post',
                url: url,
                data: $.param({
                    'type': 'getestatus'
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).
            then(function (response) {
                if (response.data.success) {
                    console.log(response.data.success);
                    if (!angular.isUndefined(response.data.data)) {
                        $scope.estatus = response.data.data;
                        console.log($scope.estatus);
                    } else {
                        $scope.estatus = [];
                        console.log($scope.estatus);
                    }
                    $scope.stopwaiting();
                    //$scope.tableParams.reload();
                } else {
                    $scope.estatus = [];
                    //$scope.tableParams.reload();
                    SweetAlert.swal({
                        title: "Error",
                        text: response.data.message,
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
                $http({
                    method: 'post',
                    url: url,
                    data: $.param({
                        'type': 'getagenda',
                        'user':  $rootScope.user.user,
                        'fecha': $scope.formattedDate(null,5)
                    }),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }).
                then(function (response) {
                    if (response.data.success) {
                        console.log(response.data.success);
                        if (!angular.isUndefined(response.data.data)) {
                            $scope.agenda = response.data.data;
                            console.log("Informacion de la Agenda");
                            console.log($scope.agenda);
                        } else {
                            $scope.agenda = [];
                            console.log($scope.agenda);
                        }
                        $scope.stopwaiting();
                        $scope.tableParamsA.reload();
                    } else {
                        $scope.agenda = [];
                        $scope.tableParamsA.reload();
                        SweetAlert.swal({
                            title: "Error",
                            text: response.data.message,
                            type: "error",
                            confirmButtonColor: "#5cb85c"
                        });
                    }
                    $http({
                        method: 'post',
                        url: url,
                        data: $.param({
                            'type' : 'gethistorialdia', 
                            'user' :  $rootScope.user.user, 
                            'fecha':  $scope.formattedDate(null,5) 
                        }),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    }).
                    then(function (response) {
                        if (response.data.success) {
                            console.log(response.data.success);
                            if (!angular.isUndefined(response.data.data)) {
                                $scope.historiaHoy = response.data.data;
                                console.log($scope.historiaHoy);
                            } else {
                                $scope.historiaHoy = [];
                                console.log($scope.historiaHoy);
                            }
                            $scope.stopwaiting();
                            $scope.tableParamsHh.reload();
                        } else {
                            $scope.historiaHoy = [];
                            $scope.tableParamsHh.reload();
                            SweetAlert.swal({
                                title: "Error",
                                text: response.data.message,
                                type: "error",
                                confirmButtonColor: "#5cb85c"
                            });
                        } 
                        $http({
                            method: 'post',
                            url: url,
                            data: $.param({
                                'type' : 'getaccionesdiarias', 
                                'fecha': $scope.formattedDate(null,5),
                                'user' : $rootScope.user.user
                            }),
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        }).
                        then(function (response) {
                            if (response.data.success) {
                                console.log(response.data.success);
                                if (!angular.isUndefined(response.data.data)) {

                                    //********************************************************************************//

                                    for(var i=0; i<=response.data.data.length-1; i++) {
                                        switch(response.data.data[i].accion) {
                                            case 'Llamada' :
                                                $rootScope.totales.Llamadas = response.data.data[i].total;
                                                break;
                                            case 'Correo' :
                                                $rootScope.totales.Correos = response.data.data[i].total;
                                                break;
                                            case 'Cita' :
                                                $rootScope.totales.Citas = response.data.data[i].total;
                                                break;
                                            case 'Visita' :
                                                $rootScope.totales.Visitas = response.data.data[i].total;
                                                break;
                                            case 'Otra' :
                                                $rootScope.totales.Otros = response.data.data[i].total;
                                                break;
                                            case 'Contacto Efectivo' :
                                                $rootScope.totales.ContactoEfectivo = response.data.data[i].total;
                                                break;
                                        }
                                    }
                                    
                                    //********************************************************************************//

                                    $scope.accionesdiariasxusuario  = response.data.data;
                                    console.log($scope.accionesdiariasxusuario );
                                   
                                } else {
                                    $scope.accionesdiariasxusuario  = [];
                                    console.log($scope.accionesdiariasxusuario);
                                }
                                $scope.stopwaiting();
                                //$scope.tableParams.reload();
                            } else {
                                $scope.accionesdiariasxusuario  = [];
                                //$scope.tableParams.reload();
                                SweetAlert.swal({
                                    title: "Error",
                                    text: response.data.message,
                                    type: "error",
                                    confirmButtonColor: "#5cb85c"
                                });
                            } 
                            $http({
                                method: 'post',
                                url: url,
                                data: $.param({
                                    'type' : 'getusuarios'
                                }),
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            }).
                            then(function (response) {
                                if (response.data.success) {
                                    console.log(response.data.success);
                                    if (!angular.isUndefined(response.data.data)) {
                                        $scope.usuarios = response.data.data;
                                        console.log($scope.usuarios);
                                    } else {
                                        $scope.usuarios = [];
                                        console.log($scope.usuarios);
                                    }
                                    $scope.stopwaiting();
                                } else {
                                    $scope.usuarios = [];
                                    SweetAlert.swal({
                                        title: "Error",
                                        text: response.data.message,
                                        type: "error",
                                        confirmButtonColor: "#5cb85c"
                                    });
                                }
                            }, function (error) {
                                $scope.stopwaiting();
                                $scope.usuarios  = [];
                                //$scope.tableParams.reload();
                                SweetAlert.swal({
                                    title: "Error",
                                    text: error.statusText,
                                    type: "error",
                                    confirmButtonColor: "#5cb85c"
                                });
                           }); 
                        }, function (error) {
                            $scope.stopwaiting();
                            $scope.accionesdiariasxusuario  = [];
                            //$scope.tableParams.reload();
                            SweetAlert.swal({
                                title: "Error",
                                text: error.statusText,
                                type: "error",
                                confirmButtonColor: "#5cb85c"
                            });
                      }); 
                    }, function (error) {
                        $scope.stopwaiting();
                        $scope.historiaHoy = [];
                        $scope.tableParamsHh.reload();
                        SweetAlert.swal({
                            title: "Error",
                            text: error.statusText,
                            type: "error",
                            confirmButtonColor: "#5cb85c"
                        });
                  }); 
                }, function (error) {
                    $scope.stopwaiting();
                    $scope.agenda = [];
                    $scope.tableParamsA.reload();
                    SweetAlert.swal({
                        title: "Error",
                        text: error.statusText,
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                });   
            }, function (error) {
                $scope.stopwaiting();
                $scope.estatus = [];
                //$scope.tableParams.reload();
                SweetAlert.swal({
                    title: "Error",
                    text: error.statusText,
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }, function (error) {
            $scope.stopwaiting();
            $scope.contactos = [];
            //$scope.tableParamsC.reload();
            SweetAlert.swal({
                title: "Error",
                text: error.statusText,
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    //*************************************************************************************************************************//
    //*************************************************************************************************************************//


    $scope.formattedDate = function (date, estilofecha) {
        estilofecha = typeof estilofecha !== 'undefined' ? estilofecha : 1;
        var d = new Date(date || Date.now());
        var hours = '' + d.getHours(),
            minutes = '' + d.getMinutes();
        var month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        if (hours.length < 2) hours = '0' + hours;
        if (minutes.length < 2) minutes = '0' + minutes;

        switch(estilofecha) {
            case 1:
                return [year, month, day].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 5:
                return [year, month, day].join('-');
                break;
            case 3:
                return [month, day, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 2:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 4:
                return [hours, minutes].join(':');
                break;
            default:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
        }
    };

    //*************************************************************************************************************************//
    //*************************************************************************************************************************//

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    //*************************************************************************************************************************//
    //*************************************************************************************************************************//

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
        k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
        }
        return false;
    };
    
    //*************************************************************************************************************************//
    //*************************************************************************************************************************//


}]);