'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('abcontactos', ["$rootScope", "$scope", "$filter", "NgTableParams", "$http",  "$modal", "$log", "SweetAlert", "$interval", "toaster", "uiCalendarConfig", '$timeout', '$compile', '$stateParams', function ($rootScope, $scope, $filter, NgTableParams, $http, $modal, $log, SweetAlert, $interval, toaster, uiCalendarConfig, $timeout, $compile, $stateParams) {
    
    var base_path = document.getElementById('base_path').value;
    var url = base_path + 'assets/js/php/abcontactos_DB.php';
    
    
    //****************************************************************************************//

    $scope.contactos         = [];
    $scope.tempContacto      = {};
    $scope.tipoactualizacion = '';
    $scope.editar            = false;

    
    //****************************************************************************************//

    $scope.tableParams = new NgTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            descontacto: 'asc' // initial sorting
        }
    }, {
        counts: [], // Hides page sizes
        total: $scope.contactos.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.contactos, params.filter()) : $scope.contactos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    //****************************************************************************************//
    $scope.init = function () {
        $scope.waiting();
        $http({
            method: 'post',
            url: url,
            data: $.param({
                'type' : 'getcontactos',
                'user' : $rootScope.user
            }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).
        then(function (response) {
            console.log(response);
            if (response.data.success) {
                console.log(response.data.success);
                if (!angular.isUndefined(response.data.data)) {
                    $scope.contactos = response.data.data;
                    console.log("Contactos");
                    console.log($scope.contactos);
                } else {
                    $scope.contactos = [];
                    console.log("contactos");
                    console.log($scope.contactos);
                }
                $scope.stopwaiting();
                $scope.tableParams.reload();
            } else {
                $scope.agendacalendario = [];
                $scope.tableParams.reload();
                SweetAlert.swal({
                    title: "Error",
                    text: response.data.message,
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }, function (error) {
            $scope.stopwaiting();
            $scope.agenda_calendario = [];
            $scope.tableParams.reload();
            SweetAlert.swal({
                title: "Error",
                text: error.statusText,
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    
    //****************************************************************************************//

    $scope.agrega_contacto = function () {
        $scope.tempContacto = {
            descontacto: '',
            desfuente: '',
            telefono: '',
            email: ''
        }
        $scope.tipoactualizacion = "inserta"
        $scope.editar = true;       
    }


    $scope.editar_contacto = function(Contactos) {
        console.log(Contactos);
        $scope.tempContacto = $.extend({}, Contactos);
        $scope.indexContacto = getIndexOf($scope.contactos,$scope.tempContacto.idcontacto,'idcontacto');
        $scope.tipoactualizacion = "edicion"
        $scope.editar = true;
    }

    //****************************************************************************************//
    //Guardar  o Edicion de Contacto//
    $scope.guarda_contacto = function() {
        console.log($scope.tempContacto);
        $scope.waiting();
        if($scope.tipoactualizacion=="edicion") {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 
                  'type' : 'savecontacto', 
                  'contacto' : $scope.tempContacto,
                  'user': $rootScope.user.user   
              }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            then(function(response) {
                if(response.data.success) {
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: response.data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.editar = false;
                    $scope.stopwaiting();
                } else {
                    SweetAlert.swal({
                        title: "Error", 
                        text: response.data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
                $scope.init();
                $scope.stopwaiting();
            }, function(error) {
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: error.statusText, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        } else {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 
                  'type' : 'savecontacto', 
                  'contacto': $scope.tempContacto,
                  'user' : $rootScope.user.user
              }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            then(function(response) {
                if(response.data.success) {
                    $scope.contactos.push($scope.tempContacto);
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: response.data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.editar = false;
                    $scope.tableParams.reload();
                    $scope.stopwaiting();
                } else {
                    SweetAlert.swal({
                        title: "Error", 
                        text: response.data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
                $scope.init();
                $scope.stopwaiting();
            }, function(error) {
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: error.statusText, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }
    }
        
    //****************************************************************************************//


    $scope.formattedDate = function (date, estilofecha) {
        estilofecha = typeof estilofecha !== 'undefined' ? estilofecha : 1;
        var d = new Date(date || Date.now());
        var hours = '' + d.getHours(),
            minutes = '' + d.getMinutes();
        var month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        if (hours.length < 2) hours = '0' + hours;
        if (minutes.length < 2) minutes = '0' + minutes;

        switch(estilofecha) {
            case 1:
                return [year, month, day].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 5:
                return [year, month, day].join('-');
                break;
            case 3:
                return [month, day, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 2:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 4:
                return [hours, minutes].join(':');
                break;
            default:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
        }
    };

    //****************************************************************************************//

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    //****************************************************************************************//

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
        k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
        }
        return false;
    };
    
    //****************************************************************************************//


}]);