'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('metasmensual', ["$rootScope", "$scope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", "$interval", "toaster", "uiCalendarConfig", '$timeout', '$compile', '$stateParams', function ($rootScope, $scope, $filter, ngTableParams, $http, $modal, $log, SweetAlert, $interval, toaster, uiCalendarConfig, $timeout, $compile, $stateParams) {
    $scope.tipocalendario = $stateParams.tipo;
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
   

    $scope.post.Metas = [];
    $scope.post.MetasTotal = [
        {
            shortname: 'Llamadas',
            real_t: 0,
            meta_t: 0, 
            dife_t: 0
        },
        {
            shortname: 'Citas',
            real_t: 0,
            meta_t: 0, 
            dife_t: 0
        },
        {
            shortname: 'Monto',
            real_t: 0,
            meta_t: 0, 
            dife_t: 0
        },
        {
            shortname: 'Diaria',
            diar_ll: 0,
            diar_ce: 0
        },
    ];
   
    $scope.tempMetas = {};
   
    $scope.wait = "";
    $scope.Timer = null;
    $scope.Timer1 = null;
    $scope.Timer2 = null;
    $scope.Timer3 = null;
    
    $scope.mostrar = 'inicial',
    $scope.agenda_calendario = {
        events : []
    };
    $scope.post.metas_m = {
        datasets: [
                {
                    fillColor: 'rgba(255,102,0,0.5)',
                    strokeColor: 'rgba(255,102,0,0.8)',
                    highlightFill: 'rgba(255,102,0,0.75)',
                    highlightStroke: 'rgba(255,102,0,1)',
                    data: []
                },
            ]
    };
    $scope.post.metas_di = {
        datasets: [
            {
                label: 'Llamadas',
                fillColor: 'rgba(220,220,220,0.5)',
                strokeColor: 'rgba(220,220,220,0.8)',
                highlightFill: 'rgba(220,220,220,0.75)',
                highlightStroke: 'rgba(220,220,220,1)',
                data: []
            },
            {
                label: 'Contactos Efectivos',
                fillColor: 'rgba(255,102,0,0.5)',
                strokeColor: 'rgba(255,102,0,0.8)',
                highlightFill: 'rgba(255,102,0,0.75)',
                highlightStroke: 'rgba(255,102,0,1)',
                data: []
            },
        ]
    };
    $scope.post.metas_ll = angular.copy($scope.post.metas_m);
    $scope.post.metas_co = angular.copy($scope.post.metas_m);
    $scope.post.metas_v = angular.copy($scope.post.metas_m);
    $scope.post.metas_ci = angular.copy($scope.post.metas_m);
    $scope.post.metas_to = angular.copy($scope.post.metas_m);
    $scope.optionscurrency = {
        maintainAspectRatio: false,
        responsive: true,
        scaleShowGridLines: true,
        scaleGridLineColor: 'rgba(0,0,0,.05)',
        scaleGridLineWidth: 1,
        bezierCurve: true,
        bezierCurveTension: 0.4,
        pointDot: true,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 2,
        datasetFill: true,
        onAnimationProgress: function () { },
        onAnimationComplete: function () { },
        //legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
        scaleLabel:
        function(label){return  ' $' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
    };
    $scope.options = {
        maintainAspectRatio: false,
        responsive: true,
        scaleShowGridLines: true,
        scaleGridLineColor: 'rgba(0,0,0,.05)',
        scaleGridLineWidth: 1,
        bezierCurve: true,
        bezierCurveTension: 0.4,
        pointDot: true,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 2,
        datasetFill: true,
        barDatasetSpacing: 10,
        onAnimationProgress: function () { },
        onAnimationComplete: function () { },
        legend: {
            display: false
        },
        legendCallback: function (chart) {
            // Return the HTML string here.
            var legendTemplate = '<ul class="tc-chart-js-legend">';
            for (var i = 0; i < chart.legend.legendItems.length; i++) {
                var legendItem = chart.legend.legendItems[i];
                legendTemplate = legendTemplate + '<li><span style="background-color:' + legendItem.strokeStyle + '"></span>' + legendItem.text + '</li>';
            }
            legendTemplate = legendTemplate + '</ul>';
            return legendTemplate;
        }
    };



     var url = base_path+'assets/js/php/metasmensual_DB.php';

     //****************************************************************************************************************//
     //****************************************************************************************************************//
    $scope.tableParamsMetas_m = new ngTableParams({
        count: 1000,
        sorting: {
            real_m: 'desc' // initial sorting
        },
    }, {
        counts: [],
        getData: function ($defer, params) {
            var orderedData = $filter('orderBy')($scope.post.Metas, params.orderBy());
            params.total(orderedData);
            $defer.resolve(orderedData);
        }
    });

     //****************************************************************************************************************//
     //****************************************************************************************************************//
    $scope.tableParamsMetas_di = new ngTableParams({
        count: 1000,
        sorting: {
            name: 'asc' // initial sorting
        },
    }, {
        counts: [],
        getData: function ($defer, params) {
            var orderedData = $filter('orderBy')($scope.post.Metas, params.orderBy());
            params.total(orderedData);
            $defer.resolve(orderedData);
        }
    });

    //****************************************************************************************************************//
    //****************************************************************************************************************//
    $scope.tableParamsMetas_ll = new ngTableParams({
        count: 1000,
        sorting: {
            real_ll: 'desc' // initial sorting
        },
    }, {
        counts: [],
        getData: function ($defer, params) {
            var orderedData = $filter('orderBy')($scope.post.Metas, params.orderBy());
            params.total(orderedData);
            $defer.resolve(orderedData);
        }
    });
    
     //****************************************************************************************************************//
     //****************************************************************************************************************//

    $scope.tableParamsMetas_ci = new ngTableParams({
        count: 1000,
        sorting: {
            real_ci: 'desc' // initial sorting
        },
    }, {
        counts: [],
        getData: function ($defer, params) {
            var orderedData = $filter('orderBy')($scope.post.Metas, params.orderBy());
            params.total(orderedData);
            $defer.resolve(orderedData);
        }
    });

     //****************************************************************************************************************//
     //****************************************************************************************************************//
    $scope.tableParamsMetas_co = new ngTableParams({
        count: 1000
    }, {
        counts: [],
        getData: function ($defer, params) {
            params.total($scope.post.Metas);
            $defer.resolve($scope.post.Metas);
        }
    });
     //****************************************************************************************************************//
     //****************************************************************************************************************//
    $scope.tableParamsMetas_v = new ngTableParams({
        count: 1000
    }, {
        counts: [],
        getData: function ($defer, params) {
            params.total($scope.post.Metas);
            $defer.resolve($scope.post.Metas);
        }
    });

     //****************************************************************************************************************//
     //****************************************************************************************************************//
    $scope.tableParamsMetas_ci = new ngTableParams({
        count: 1000,
        sorting: {
            real_ci: 'desc' // initial sorting
        },
    }, {
        counts: [],
        getData: function ($defer, params) {
            var orderedData = $filter('orderBy')($scope.post.Metas, params.orderBy());
            params.total(orderedData);
            $defer.resolve(orderedData);
        }
    });

     //****************************************************************************************************************//
    //****************************************************************************************************************//
    $scope.tableParamsMetas_to = new ngTableParams({
        count: 1000
    }, {
        counts: [],
        getData: function ($defer, params) {
            params.total($scope.post.MetasTotal);
            $defer.resolve($scope.post.MetasTotal);
        }
    });

   //****************************************************************************************************************//
   //****************************************************************************************************************//
    $scope.init_metas2 = function(origen){
    $scope.waiting();

    if($scope.Timer2)
        $interval.cancel($scope.Timer2);   
    if($scope.Timer3)
        $interval.cancel($scope.Timer3);   

    $http({
      method: 'post',
      url: url,
      data: $.param({ 'type' : 'getmetas2' }),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).
    success(function(data, status, headers, config) {
        if(data.success && !angular.isUndefined(data.data) ){
            $scope.post.Metas = data.data;
            $scope.post.metas_m.labels = [];
            $scope.post.metas_di.labels = [];
            $scope.post.metas_ll.labels = [];
            $scope.post.metas_ci.labels = [];
            $scope.post.metas_to.labels = [];
            $scope.post.MetasTotal[0].real_t = 0;
            
            // orden de llamadas
            angular.forEach($filter('orderBy')($scope.post.Metas, '-real_ll' ), function(metas, metas_key) {
                $scope.post.metas_ll.labels.push(metas.shortname);
                $scope.post.metas_ll.datasets[0].data.push(metas.real_ll);
                $scope.post.MetasTotal[0].real_t = $scope.post.MetasTotal[0].real_t + metas.real_ll;
                $scope.post.MetasTotal[0].meta_t = $scope.post.MetasTotal[0].meta_t + metas.meta_ll;
            });

            // orden de citas
            angular.forEach($filter('orderBy')($scope.post.Metas, '-real_ci' ), function(metas, metas_key) {
                $scope.post.metas_ci.labels.push(metas.shortname);
                $scope.post.metas_ci.datasets[0].data.push(metas.real_ci);
                $scope.post.MetasTotal[1].real_t = $scope.post.MetasTotal[1].real_t + metas.real_ci;
                $scope.post.MetasTotal[1].meta_t = $scope.post.MetasTotal[1].meta_t + metas.meta_ci;
            });

            // orden de montos
            angular.forEach($filter('orderBy')($scope.post.Metas, '-real_m' ), function(metas, metas_key) {
                $scope.post.metas_m.labels.push(metas.shortname);
                $scope.post.metas_m.datasets[0].data.push(metas.real_m);
                $scope.post.MetasTotal[2].real_t = $scope.post.MetasTotal[2].real_t + metas.real_m;
                $scope.post.MetasTotal[2].meta_t = $scope.post.MetasTotal[2].meta_t + metas.meta_m;
            });

            // orden de llamadas diarias
            angular.forEach($filter('orderBy')($scope.post.Metas, 'name' ), function(metas, metas_key) {
                $scope.post.metas_di.labels.push(metas.shortname);
                $scope.post.metas_di.datasets[0].data.push(metas.diar_ll);
                $scope.post.metas_di.datasets[1].data.push(metas.diar_ce);
                $scope.post.MetasTotal[3].diar_ll = $scope.post.MetasTotal[3].diar_ll + metas.diar_ll;
                $scope.post.MetasTotal[3].diar_ce = $scope.post.MetasTotal[3].diar_ce + metas.diar_ce;
            });


            angular.forEach($scope.post.MetasTotal, function(metas, metas_key) {
                $scope.post.metas_to.labels.push(metas.shortname);
                $scope.post.metas_to.datasets[0].data.push(metas.real_t);
            });
            $scope.tableParamsMetas_m.reload();
            $scope.tableParamsMetas_di.reload();
            $scope.tableParamsMetas_ll.reload();
            $scope.tableParamsMetas_co.reload();
            $scope.tableParamsMetas_v.reload();
            $scope.tableParamsMetas_ci.reload();
            $scope.tableParamsMetas_to.reload();
            //$scope.StartTimer_d3();
            //$scope.StartTimer_d3_1();
            $scope.mostrar="datos";
        } else {
            $scope.post.Metas = [];
            $scope.tableParamsMetas_m.reload();
            $scope.tableParamsMetas_di.reload();
            $scope.tableParamsMetas_ll.reload();
            $scope.tableParamsMetas_ci.reload();
        }
        $scope.stopwaiting();
    }).
    error(function(data, status, headers, config) {
        $scope.waiting();
        SweetAlert.swal({
            title: "Error", 
            text: data.message, 
            type: "error",
            confirmButtonColor: "#5cb85c"
        });
    });
    }

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    $scope.StartTimer = function () {
        var i = 0;
        $scope.Timer = $interval(function () {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getproximoscontactos', 'user' : $rootScope.user.user }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success && !angular.isUndefined(data.data) ){
                    for(i = 0; i <= data.data.length-1; i++) {
                        toaster.pop('warning','Aviso de contacto','<br>' + data.data[i].desempresa + ' - <strong> ' + $scope.formattedDate(data.data[i].fecsigcont,4),60000,'trustedHtml');
                    }
                }
            });
        }, 90000);
    }

    $scope.StartTimer_d1 = function () {
        $scope.Timer1 = $interval(function () {
            $scope.waiting();
            $scope.agenda_d1();
            $scope.agenda_annio_d1();
            $scope.historialhoy_d1();
            $scope.stopwaiting();
        }, 60000);
    }

    $scope.StartTimer_d3 = function () {
        $scope.Timer2 = $interval(function () {
            if($scope.mostrar=='datos')
                $scope.mostrar = 'grafica';
            else
                $scope.mostrar = 'datos';
        }, 6000);
    }

    $scope.StartTimer_d3_1 = function () {
        $scope.Timer3 = $interval(function () {
            $scope.mostrar = 'grafica';
            $scope.init_metas2();
        }, 6000);
    }

    $scope.$on('$destroy',function(){
        if($scope.Timer)
            $interval.cancel($scope.Timer);   
        if($scope.Timer1)
            $interval.cancel($scope.Timer1);   
        if($scope.Timer2)
            $interval.cancel($scope.Timer2);   
        if($scope.Timer3)
            $interval.cancel($scope.Timer3);   
    });

   

}]);