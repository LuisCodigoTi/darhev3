'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('tipificacion', ["$rootScope", "$scope", "$filter", "NgTableParams", "$http",  "$modal", "$log", "SweetAlert", "$interval", "toaster", "uiCalendarConfig", '$timeout', '$compile', '$stateParams', function ($rootScope, $scope, $filter, NgTableParams, $http, $modal, $log, SweetAlert, $interval, toaster, uiCalendarConfig, $timeout, $compile, $stateParams) {
    
    var base_path = document.getElementById('base_path').value;
    var url = base_path + 'assets/js/php/tipificacion_BD.php';
    
   
    $scope.post = {};
    $scope.post.TotalXTipif = [];
    $scope.post.TotalXTipifEnc = [];
    $scope.post.usuarios = [];
    $scope.post.usuario = '';
  
   
    $scope.wait = "";
    $scope.Timer = null;
    $scope.Timer1 = null;
    $scope.Timer2 = null;
    $scope.Timer3 = null;
    $scope.post.filtro = [];
  

   
    $scope.init_tipif2 = function(){
        $scope.waiting();
        $http({
            method: 'post',
            url: url,
            data: $.param({ 'type' : 'getusuarios' }),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.usuarios = data.data;
            }
            $scope.post.usuario = 'TODOS';
            $scope.dashboardtipif();
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.waiting();
            SweetAlert.swal({
                title: "Error 2", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.dashboardtipif = function() {
        $scope.waiting();
        var rutina = '';
        if($scope.post.usuario == 'TODOS') rutina='getcontactostipif2';
        else  rutina='getcontactostipif3';
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : rutina, 'user' : $scope.post.usuario }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.TotalXTipif = data.data;
                $scope.post.TotalXTipifEnc = [];
                for(var i=0; i<=$scope.post.TotalXTipif.length-1; ++i) {
                    if($scope.post.TotalXTipif[i].desestatus=='TOTAL REGISTROS BASE DE DATOS') {
                        $scope.post.TotalXTipifEnc.push($scope.post.TotalXTipif[i]);
                    }
                }
                for(var i=0; i<=$scope.post.TotalXTipif.length-1; ++i) {
                    var encontrado = false;
                    for(var j=0; j<=$scope.post.TotalXTipifEnc.length-1; ++j) {
                        if($scope.post.TotalXTipif[i].fuente == $scope.post.TotalXTipifEnc[j].fuente) {
                            encontrado = true;
                            break;
                        }
                    }
                    if(encontrado) $scope.post.TotalXTipif[i].porcentaje = $scope.post.TotalXTipif[i].total / $scope.post.TotalXTipifEnc[j].total * 100;
                }
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.waiting();
            SweetAlert.swal({
                title: "Error 2", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }


    $scope.formattedDate = function (date, estilofecha) {
        estilofecha = typeof estilofecha !== 'undefined' ? estilofecha : 1;
        var d = new Date(date || Date.now());
        var hours = '' + d.getHours(),
            minutes = '' + d.getMinutes();

        var month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        if (hours.length < 2) hours = '0' + hours;
        if (minutes.length < 2) minutes = '0' + minutes;

        switch(estilofecha) {
            case 1:
                return [year, month, day].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 5:
                return [year, month, day].join('-');
                break;
            case 3:
                return [month, day, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 2:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 4:
                return [hours, minutes].join(':');
                break;
            default:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
        }
    };

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
        k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
        }
        return false;
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }



}]);

