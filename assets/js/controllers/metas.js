'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('metas', ["$rootScope", "$scope", "$filter", "NgTableParams", "$http",  "$modal", "$log", "SweetAlert", "$interval", "toaster", "uiCalendarConfig", '$timeout', '$compile', '$stateParams', function ($rootScope, $scope, $filter, NgTableParams, $http, $modal, $log, SweetAlert, $interval, toaster, uiCalendarConfig, $timeout, $compile, $stateParams) {
    
    var base_path = document.getElementById('base_path').value;
    var url = base_path + 'assets/js/php/metas_DB.php';
    
    $scope.editarmetas     = false;
    
    //*************************************************************************************************************************//
    //*************************************************************************************************************************//
  
    $scope.tempMetas = {};

    $scope.metas = [];
    $scope.resmetas  = [];

    //*************************************************************************************************************************//
    //*************************************************************************************************************************//

    $scope.tableParams = new NgTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            descontacto: 'asc' // initial sorting
        }
    }, {
        counts: [], // Hides page sizes
        total: $scope.resmetas.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.resmetas, params.filter()) : $scope.resmetas;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
    
    //*************************************************************************************************************************//
    //*************************************************************************************************************************//

    $scope.init = function () {
        $scope.waiting();
         $http({
            method: 'post',
            url: url,
            data: $.param({
                'type' : 'getmetas'
                }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
             }
            }).
            then(function (response) {
                if (response.data.success) {
                    console.log(response.data.success);
                    if (!angular.isUndefined(response.data.data)) {
                        console.log("INIT METAS");
                        $scope.resmetas  = response.data.data;
                        console.log($scope.resmetas);           
                        } else {
                            $scope.resmetas  = [];
                            console.log($scope.resmetas);
                          }
                        $scope.stopwaiting();
                        $scope.tableParams.reload();
                    } else {
                        $scope.resmetas = [];
                        $scope.tableParams.reload();
                        SweetAlert.swal({
                                title: "Error",
                                text: response.data.message,
                                type: "error",
                                confirmButtonColor: "#5cb85c"
                        });
                      } 
                    }, function (error) {
                            $scope.stopwaiting();
                            $scope.resmetas  = [];
                            $scope.tableParams.reload();
                            SweetAlert.swal({
                                title: "Error",
                                text: error.statusText,
                                type: "error",
                                confirmButtonColor: "#5cb85c"
                            });
                }); 
        }

    //**********************************************************************//
    
    $scope.edita_metas = function(Metas) {

        $scope.tempMetas = {
            name:      Metas.user,
            monto:     '',
            llamadas : '',
            correos:   '',
            visitas:   '',
            citas:     ''
           
        }

        console.log($scope.tempMetas);
        $scope.editarmetas = true;
    }
  
    //**********************************************************************//
    //Guardar Metas//
    $scope.savemetas = function() {
            console.log($scope.tempMetas);
            $scope.waiting();
            $http({
              method: 'post',
              url: url,
              data: $.param({ 
                  'type' : 'save_metas', 
                  'metas': $scope.tempMetas
              }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            then(function(response) {
                console.log(response);
                if(response.data.success) {
                    if( $scope.editMode ){
                        $scope.metas[$scope.indexM].monto    = $scope.tempMetas.monto;
                        $scope.metas[$scope.indexM].llamadas = $scope.tempMetas.llamadas;
                        $scope.metas[$scope.indexM].correos  = $scope.tempMetas.correos;
                        $scope.metas[$scope.indexM].visitas  = $scope.tempMetas.visitas;
                        $scope.metas[$scope.indexM].citas    = $scope.tempMetas.citas;
                    }
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: response.data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.editarmetas = false;
                    $scope.init();
                    $scope.tableParams.reload();
                    $scope.stopwaiting();
                } else {
                    SweetAlert.swal({
                        title: "Error", 
                        text: response.data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
                $scope.init();
                $scope.stopwaiting();
            }, function(error) {
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: error.statusText, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
    }        
    //**********************************************************************//
    //**********************************************************************//
    $scope.formattedDate = function (date, estilofecha) {
        estilofecha = typeof estilofecha !== 'undefined' ? estilofecha : 1;
        var d = new Date(date || Date.now());
        var hours = '' + d.getHours(),
            minutes = '' + d.getMinutes();
/*            d.setMinutes(d.getMinutes() + d.getTimezoneOffset());*/

        var month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        if (hours.length < 2) hours = '0' + hours;
        if (minutes.length < 2) minutes = '0' + minutes;

        switch(estilofecha) {
            case 1:
                return [year, month, day].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 5:
                return [year, month, day].join('-');
                break;
            case 3:
                return [month, day, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 2:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 4:
                return [hours, minutes].join(':');
                break;
            default:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
        }
    };

    //****************************************************************************************//

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
        k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
        }
        return false;
    };

}]);