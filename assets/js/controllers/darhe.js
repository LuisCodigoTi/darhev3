'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('darhe', ["$rootScope", "$scope", "$filter", "NgTableParams", "$http",  "$modal", "$log", "SweetAlert", "$interval", "toaster", "uiCalendarConfig", '$timeout', '$compile', '$stateParams', function ($rootScope, $scope, $filter, NgTableParams, $http, $modal, $log, SweetAlert, $interval, toaster, uiCalendarConfig, $timeout, $compile, $stateParams) {
    
    var base_path = document.getElementById('base_path').value;
    var url = base_path + 'assets/js/php/darhe_DB.php';
    
    $scope.informacion   = false;
    $scope.nuevocontacto = false;
    $scope.actividad     = false;
    $scope.asignar       = false;

    $scope.contactos = [];
    $scope.historial = [];
    $scope.guardausuario = [];
    $scope.datoscontactos = [];
    $scope.estatus = [];
    $scope.agenda = [];
    $scope.historiaHoy = [];
    $scope.accionesdiariasxusuario = [];
    $scope.usuarios = [];

    $scope.tipoactualizacion = '';
    $scope.indexContacto = 0;
    $scope.selectedRowC = 0;
    $scope.tempAccion = {};
    $scope.tempContacto = {};
    $scope.auxiliar = {};

    //*************************************************************************************************************************//
    //*************************************************************************************************************************//

    $scope.tableParamsC = new NgTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            descontacto: 'asc' // initial sorting
        }
    }, {
        counts: [], // Hides page sizes
        total: $scope.contactos.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.contactos, params.filter()) : $scope.contactos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    //*************************************************************************************************************************//
    //*************************************************************************************************************************//

    $scope.tableParamsH = new NgTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            descontacto: 'asc' // initial sorting
        }
    }, {
        counts: [], // Hides page sizes
        total: $scope.datoscontactos.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.datoscontactos, params.filter()) : $scope.datoscontactos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    //*************************************************************************************************************************//
    //*************************************************************************************************************************//
   
    $scope.tableParamsA = new NgTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            descontacto: 'asc' // initial sorting
        }
    }, {
        counts: [], // Hides page sizes
        total: $scope.agenda.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.agenda, params.filter()) : $scope.agenda;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });


    //*************************************************************************************************************************//
    //*************************************************************************************************************************//

    $scope.tableParamsHh = new NgTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            descontacto: 'asc' // initial sorting
        }
    }, {
        counts: [], // Hides page sizes
        total: $scope.historiaHoy.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.historiaHoy, params.filter()) : $scope.historiaHoy;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });


    //*************************************************************************************************************************//
    //*************************************************************************************************************************//

    $scope.tableParams = new NgTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            descontacto: 'asc' // initial sorting
        }
    }, {
        counts: [], // Hides page sizes
        total: $scope.estatus.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.estatus, params.filter()) : $scope.estatus;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });


    //*************************************************************************************************************************//
    //*************************************************************************************************************************//
    $scope.init = function () {
        console.log("LOGIN DE USUARIO");
        console.log($rootScope.user);
        $scope.waiting();
        $http({
            method: 'post',
            url: url,
            data: $.param({
                'type': 'getcontactos',
                'user': $rootScope.user
            }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).
        then(function (response) {
            if (response.data.success) {
                console.log(response.data.success);
                if (!angular.isUndefined(response.data.data)) {
                    $scope.contactos = response.data.data;
                    console.log($scope.contactos);
                } else {
                    $scope.contactos = [];
                    console.log($scope.contactos);
                }
                $scope.stopwaiting();
                $scope.tableParamsC.reload();
            } else {
                $scope.contactos = [];
                $scope.tableParamsC.reload();
                SweetAlert.swal({
                    title: "Error",
                    text: response.data.message,
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
            $http({
                method: 'post',
                url: url,
                data: $.param({
                    'type': 'getestatus'
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).
            then(function (response) {
                if (response.data.success) {
                    console.log(response.data.success);
                    if (!angular.isUndefined(response.data.data)) {
                        $scope.estatus = response.data.data;
                        console.log($scope.estatus);
                    } else {
                        $scope.estatus = [];
                        console.log($scope.estatus);
                    }
                    $scope.stopwaiting();
                    $scope.tableParams.reload();
                } else {
                    $scope.estatus = [];
                    $scope.tableParams.reload();
                    SweetAlert.swal({
                        title: "Error",
                        text: response.data.message,
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
                $http({
                    method: 'post',
                    url: url,
                    data: $.param({
                        'type': 'getagenda',
                        'user':  $rootScope.user.user,
                        'fecha': $scope.formattedDate(null,5)
                    }),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }).
                then(function (response) {
                    if (response.data.success) {
                        console.log(response.data.success);
                        if (!angular.isUndefined(response.data.data)) {
                            $scope.agenda = response.data.data;
                            console.log($scope.agenda);
                        } else {
                            $scope.agenda = [];
                            console.log($scope.agenda);
                        }
                        $scope.stopwaiting();
                        $scope.tableParamsA.reload();
                    } else {
                        $scope.agenda = [];
                        $scope.tableParamsA.reload();
                        SweetAlert.swal({
                            title: "Error",
                            text: response.data.message,
                            type: "error",
                            confirmButtonColor: "#5cb85c"
                        });
                    }
                    $http({
                        method: 'post',
                        url: url,
                        data: $.param({
                            'type' : 'gethistorialdia', 
                            'user' :  $rootScope.user.user, 
                            'fecha':  $scope.formattedDate(null,5) 
                        }),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    }).
                    then(function (response) {
                        if (response.data.success) {
                            console.log(response.data.success);
                            if (!angular.isUndefined(response.data.data)) {
                                $scope.historiaHoy = response.data.data;
                                console.log($scope.historiaHoy);
                            } else {
                                $scope.historiaHoy = [];
                                console.log($scope.historiaHoy);
                            }
                            $scope.stopwaiting();
                            $scope.tableParamsHh.reload();
                        } else {
                            $scope.historiaHoy = [];
                            $scope.tableParamsHh.reload();
                            SweetAlert.swal({
                                title: "Error",
                                text: response.data.message,
                                type: "error",
                                confirmButtonColor: "#5cb85c"
                            });
                        } 
                        $http({
                            method: 'post',
                            url: url,
                            data: $.param({
                                'type' : 'getaccionesdiarias', 
                                'fecha': $scope.formattedDate(null,5),
                                'user' : $rootScope.user.user
                            }),
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        }).
                        then(function (response) {
                            if (response.data.success) {
                                console.log(response.data.success);
                                if (!angular.isUndefined(response.data.data)) {

                                    //********************************************************************************//

                                    for(var i=0; i<=response.data.data.length-1; i++) {
                                        switch(response.data.data[i].accion) {
                                            case 'Llamada' :
                                                $rootScope.totales.Llamadas = response.data.data[i].total;
                                                break;
                                            case 'Correo' :
                                                $rootScope.totales.Correos = response.data.data[i].total;
                                                break;
                                            case 'Cita' :
                                                $rootScope.totales.Citas = response.data.data[i].total;
                                                break;
                                            case 'Visita' :
                                                $rootScope.totales.Visitas = response.data.data[i].total;
                                                break;
                                            case 'Otra' :
                                                $rootScope.totales.Otros = response.data.data[i].total;
                                                break;
                                            case 'Contacto Efectivo' :
                                                $rootScope.totales.ContactoEfectivo = response.data.data[i].total;
                                                break;
                                        }
                                    }
                                    
                                    //********************************************************************************//

                                    $scope.accionesdiariasxusuario  = response.data.data;
                                    console.log($scope.accionesdiariasxusuario );
                                   
                                } else {
                                    $scope.accionesdiariasxusuario  = [];
                                    console.log($scope.accionesdiariasxusuario);
                                }
                                $scope.stopwaiting();
                                $scope.tableParams.reload();
                            } else {
                                $scope.accionesdiariasxusuario  = [];
                                $scope.tableParams.reload();
                                SweetAlert.swal({
                                    title: "Error",
                                    text: response.data.message,
                                    type: "error",
                                    confirmButtonColor: "#5cb85c"
                                });
                            } 
                            $http({
                                method: 'post',
                                url: url,
                                data: $.param({
                                    'type' : 'getusuarios'
                                }),
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            }).
                            then(function (response) {
                                if (response.data.success) {
                                    console.log(response.data.success);
                                    if (!angular.isUndefined(response.data.data)) {
                                        $scope.usuarios = response.data.data;
                                        console.log($scope.usuarios);
                                    } else {
                                        $scope.usuarios = [];
                                        console.log($scope.usuarios);
                                    }
                                    $scope.stopwaiting();
                                } else {
                                    $scope.usuarios = [];
                                    SweetAlert.swal({
                                        title: "Error",
                                        text: response.data.message,
                                        type: "error",
                                        confirmButtonColor: "#5cb85c"
                                    });
                                }
                            }, function (error) {
                                $scope.stopwaiting();
                                $scope.accionesdiariasxusuario  = [];
                                $scope.tableParams.reload();
                                SweetAlert.swal({
                                    title: "Error",
                                    text: error.statusText,
                                    type: "error",
                                    confirmButtonColor: "#5cb85c"
                                });
                           }); 
                        }, function (error) {
                            $scope.stopwaiting();
                            $scope.accionesdiariasxusuario  = [];
                            $scope.tableParams.reload();
                            SweetAlert.swal({
                                title: "Error",
                                text: error.statusText,
                                type: "error",
                                confirmButtonColor: "#5cb85c"
                            });
                      }); 
                    }, function (error) {
                        $scope.stopwaiting();
                        $scope.historiaHoy = [];
                        $scope.tableParamsHh.reload();
                        SweetAlert.swal({
                            title: "Error",
                            text: error.statusText,
                            type: "error",
                            confirmButtonColor: "#5cb85c"
                        });
                  }); 
                }, function (error) {
                    $scope.stopwaiting();
                    $scope.agenda = [];
                    $scope.tableParamsA.reload();
                    SweetAlert.swal({
                        title: "Error",
                        text: error.statusText,
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                });   
            }, function (error) {
                $scope.stopwaiting();
                $scope.estatus = [];
                $scope.tableParams.reload();
                SweetAlert.swal({
                    title: "Error",
                    text: error.statusText,
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }, function (error) {
            $scope.stopwaiting();
            $scope.contactos = [];
            $scope.tableParamsC.reload();
            SweetAlert.swal({
                title: "Error",
                text: error.statusText,
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    //**********************************************************************//
    //Agrega nuevo contacto//
    $scope.agrega_contacto = function () {
        $scope.tempContacto = {
            descontacto: '',
            desfuente: '',
            telefono: '',
            email: '',
            verificado: false
        }
        $scope.tipoactualizacion = "inserta"
        $scope.Form.$setPristine();
        $scope.Form.$setUntouched();
        $scope.informacion = true;       
    }

    $scope.edita_contacto = function(Contacto) {
        $scope.tempContacto = $.extend({}, Contacto);
        $scope.indexContacto = getIndexOf($scope.contactos,$scope.tempContacto.idcontacto,'idcontacto');
        $scope.tipoactualizacion = "edicion"
        $scope.informacion=true;
    }

    $scope.registra_actividad = function(Contactos) {
        console.log(Contactos);
        $scope.tempAccion = {
            tipoaccion: '',
            idestatus: '',
            montocotizado: '',
            estatusdesglosado: '',
            descontacto: Contactos.descontacto,
            desfuente: Contactos.desfuente,
            telefono: Contactos.telefono,
            email: Contactos.email,
            verificado: Contactos.verificado,
            idcontacto: Contactos.idcontacto,
            user: $rootScope.user.user,
            fechasiguientecontacto: '',
            fechacita: '',
            observacionescita: ''
        }
        console.log($scope.tempAccion);
        $scope.actividad=true;
        $scope.informacion=false;
    }

    
    $scope.visualizar_actividad = function(Historial) {
        console.log(Historial);
        $scope.tempAccion = {
            tipoaccion:        Historial.tipoaccion,
            idestatus:         Historial.idestatus,
            montocotizado:     Historial.montocotizado,
            estatusdesglosado: Historial.estatusdesglosado,
            descontacto:       Historial.descontacto,
            desfuente:         Historial.desfuente,
            telefono:          Historial.telefono,
            email:             Historial.email,
            verificado:        Historial.verificado,
            idcontacto:        Historial.idcontacto,
            user:              $rootScope.user.user,
            fechasiguientecontacto: Historial.fechasiguientecontacto,
            fechacita:              Historial.fechacita,
            observacionescita:      Historial.observacionescita
        }
        console.log($scope.tempAccion);
        $scope.actividad=true;
        $scope.informacion=false;
    }

    $scope.visualizar_actividad_contacto = function(Historial) {
        console.log(Historial);
        $scope.tempAccion = {
            tipoaccion:        Historial.tipoaccion,
            idestatus:         Historial.idestatus,
            montocotizado:     Historial.montocotizado,
            estatusdesglosado: Historial.estatusdesglosado,
            descontacto:       Historial.descontacto,
            desfuente:         Historial.desfuente,
            telefono:          Historial.telefono,
            email:             Historial.email,
            verificado:        Historial.verificado,
            idcontacto:        Historial.idcontacto,
            user:              $rootScope.user.user,
            fechasiguientecontacto: Historial.fechasiguientecontacto,
            fechacita:              Historial.fechacita,
            observacionescita:      Historial.observacionescita
        }
        console.log($scope.tempAccion);
        $scope.actividad=true;
        $scope.informacion=false;
    }

    $scope.asignar_usuario = function(Contactos) {
        console.log("Asignacion de usuario");
        console.log(Contactos);
        $scope.tempContacto = {
            idcontacto : Contactos.idcontacto,
            descontacto: Contactos.descontacto,
            fuente     : Contactos.desfuente,
            usuario    : ''
        }
        console.log($scope.tempContacto);
        $scope.actividad   = false;
        $scope.informacion = false;
        $scope.asignar     = true;
    }

    //**********************************************************************//
    //Guardar Contacto//
    $scope.guarda_contacto = function() {
        console.log($scope.tempContacto);
        $scope.waiting();
        if($scope.tipoactualizacion=="edicion") {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 
                  'type' : 'savecontacto', 
                  'contacto' : $scope.tempContacto,
                  'user': $rootScope.user.user   
              }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            then(function(response) {
                if(response.data.success) {
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: response.data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.informacion = false;
                    $scope.stopwaiting();
                } else {
                    SweetAlert.swal({
                        title: "Error", 
                        text: response.data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
                $scope.init();
                $scope.stopwaiting();
            }, function(error) {
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: error.statusText, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        } else {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 
                  'type' : 'savecontacto', 
                  'contacto': $scope.tempContacto,
                  'user' : $rootScope.user.user
              }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            then(function(response) {
                if(response.data.success) {
                    $scope.tempContacto.idaccion = response.data.idplaca;
                    $scope.contactos.push($scope.tempContacto);
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: response.data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.informacion = false;
                    $scope.tableParams.reload();
                    $scope.stopwaiting();
                } else {
                    SweetAlert.swal({
                        title: "Error", 
                        text: response.data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
                $scope.init();
                $scope.stopwaiting();
            }, function(error) {
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: error.statusText, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }
    }
    
    //**********************************************************************//
    //Guardar Actividad//
    $scope.guarda_actividad = function() {
        console.log($scope.tempAccion);
        $scope.waiting();
        $scope.tempAccion.fechasiguientecontacto = $scope.tempAccion.fechasiguientecontacto == null ? null : $scope.formattedDate($scope.tempAccion.fechasiguientecontacto);
        $scope.tempAccion.fechacita = $scope.tempAccion.fechacita == null ? null : $scope.formattedDate($scope.tempAccion.fechacita);
            $http({
              method: 'post',
              url: url,
              data: $.param({ 
                  'type' : 'saveactividad', 
                  'accion': $scope.tempAccion,
                  'user' : $rootScope.user.user
              }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            then(function(response) {
                if(response.data.success) {
                    $scope.tempAccion.idaccion = response.data.idaccion;
                    $scope.historial.push($scope.tempAccion);
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: response.data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.actividad = false;
                    $scope.init();
                    $scope.tableParams.reload();
                    $scope.stopwaiting();
                } else {
                    SweetAlert.swal({
                        title: "Error", 
                        text: response.data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
                $scope.init();
                $scope.stopwaiting();
            }, function(error) {
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: error.statusText, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
    }
    //**********************************************************************//
    //Guardar usuario//
    $scope.guarda_usuario = function() {
        console.log("Funcion para guardar el usuario");
        console.log($scope.tempContacto);
        $scope.waiting();
            $http({
              method: 'post',
              url: url,
              data: $.param({ 
                  'type' : 'saveusuario', 
                  'usuario': $scope.tempContacto
              }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            then(function(response) {
                if(response.data.success) {
                    $scope.tempAccion.idaccion = response.data.idaccion;
                    $scope.guardausuario.push($scope.tempContacto);
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: response.data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.asignar = false;
                    $scope.init();
                    $scope.stopwaiting();
                } else {
                    SweetAlert.swal({
                        title: "Error", 
                        text: response.data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
                $scope.init();
                $scope.stopwaiting();
            }, function(error) {
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: error.statusText, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
    }

    //**********************************************************************//
    $scope.contactoHistorial = function (Contactos, index) {
        
        $scope.auxiliar = {
            descontacto: Contactos.descontacto,
        };
        
        $scope.selectedRowC = index;

        console.log($scope.auxiliar);
    
        $scope.waiting();
        $http({
            method: 'post',
            url: url,
            data: $.param({
                'type': 'gethistorial',
                'descontacto': $scope.auxiliar
            }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).
        then(function (response) {
            if (response.data.success) {
                console.log(response.data.success);
                if (!angular.isUndefined(response.data.data)) {
                    $scope.datoscontactos = response.data.data;
                    console.log($scope.datoscontactos);
                } else {
                    $scope.datoscontactos = [];
                    console.log($scope.datoscontactos);
                }
                $scope.stopwaiting();
                $scope.tableParamsH.reload();
            } else {
                $scope.datoscontactos = [];
                $scope.tableParamsH.reload();
                SweetAlert.swal({
                    title: "Error",
                    text: response.data.message,
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }, function (error) {
            console.log(error);
            $scope.stopwaiting();
            $scope.datoscontactos = [];
            $scope.tableParamsH.reload();
            SweetAlert.swal({
                title: "Error",
                text: error.statusText,
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    //**********************************************************************//
    //**********************************************************************//
    $scope.formattedDate = function (date, estilofecha) {
        estilofecha = typeof estilofecha !== 'undefined' ? estilofecha : 1;
        var d = new Date(date || Date.now());
        var hours = '' + d.getHours(),
            minutes = '' + d.getMinutes();
/*            d.setMinutes(d.getMinutes() + d.getTimezoneOffset());*/

        var month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        if (hours.length < 2) hours = '0' + hours;
        if (minutes.length < 2) minutes = '0' + minutes;

        switch(estilofecha) {
            case 1:
                return [year, month, day].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 5:
                return [year, month, day].join('-');
                break;
            case 3:
                return [month, day, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 2:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 4:
                return [hours, minutes].join(':');
                break;
            default:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
        }
    };

    //****************************************************************************************//

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
        k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
        }
        return false;
    };

}]);



