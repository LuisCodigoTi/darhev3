'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('calendario', ["$rootScope", "$scope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", "$interval", "toaster", "uiCalendarConfig", '$timeout', '$compile', '$stateParams', function ($rootScope, $scope, $filter, ngTableParams, $http, $modal, $log, SweetAlert, $interval, toaster, uiCalendarConfig, $timeout, $compile, $stateParams) {
    
    var base_path = document.getElementById('base_path').value;
    var url = base_path + 'assets/js/php/calendario_DB.php';
    
    
    //****************************************************************************************//
    //****************************************************************************************//

    $scope.tipocalendario = $stateParams.tipo;
    console.log("TIPO CALENDARIO");
    console.log($scope.tipocalendario);

  
      
    //****************************************************************************************//
    $scope.init = function () {
        $scope.waiting();
        $http({
            method: 'post',
            url: url,
            data: $.param({
                'type' : 'getagenda_calendario',
                'fecha':  $scope.formattedDate(null,5),
                'user' :  $rootScope.user
            }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).
        then(function (response) {
            console.log(response);
            if (response.data.success) {
                console.log(response.data.success);
                if (!angular.isUndefined(response.data.data)) {
                    $scope.agendacalendario = response.data.data;
                    console.log("getagenda_calendario");
                    console.log($scope.agendacalendario);
                } else {
                    $scope.agendacalendario = [];
                    console.log("getagenda_calendario");
                    console.log($scope.agendacalendario);
                }
                $scope.stopwaiting();
                //$scope.tableParams.reload();
            } else {
                $scope.agendacalendario = [];
                //$scope.tableParams.reload();
                SweetAlert.swal({
                    title: "Error",
                    text: response.data.message,
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
            //******************************************//
            //**Segunda funcion del init**//
            var color = "";
            if($scope.tipocalendario == 'ejecutivo') {
                $scope.usuarioscalendario.push($rootScope.user.name);
            } else {
                $scope.usuarioscalendario.push('Todos');
            }
            $scope.filtrocalendario.ejecutivo = $scope.usuarioscalendario[0];
            console.log($scope.filtrocalendario.ejecutivo);

            $http({
                method: 'post',
                url: url,
                data: $.param({
                    'type': 'getagenda_calendarioyear',
                    'user': $rootScope.user
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).
            then(function (response) {
               // $scope.agenda_calendario.events = [];
                if (response.data.success) {
                    $scope.agenda_calendario.events = [];
                    console.log(response.data.success);
                    if (!angular.isUndefined(response.data.data)) {
                        $scope.agenda_calendarioyear = response.data.data;
                        console.log("getagenda_calendarioyear")
                        console.log($scope.agenda_calendarioyear);
                        for(var i=0; i<=response.data.data.length-1; ++i) {
                            if(!response.data.data[i].descontacto) response.data.data[i].descontacto = ''; else response.data.data[i].descontacto = ' - ' + response.data.data[i].descontacto;
                            if(!response.data.data[i].estatusdesglosado) response.data.data[i].estatusdesglosado = ''; else response.data.data[i].estatusdesglosado = ' - ' + response.data.data[i].estatusdesglosado;
                            if(response.data.data[i].tipo_evento == 'Contacto'){ color = "#909090";}
                            else {  color = "#e65c00"; }
                            if($scope.tipocalendario == 'administrador') $scope.agregausuariofiltrocalendario(response.data.data[i].name);
                            $scope.agenda_calendario.events.push({
                                title: response.data.data[i].descontacto,
                                tooltip: response.data.data[i].descontacto + response.data.data[i].estatusdesglosado + ' - Ejecutivo: ' + response.data.data[i].name,
                                tipoevento: response.data.data[i].tipo_evento,
                                ejecutivo: response.data.data[i].name,
                                start: $scope.formattedDate(response.data.data[i].fecha,1),
                                color: color,
                                allDay: false
                            });
                        }
                    } else {
                        $scope.agenda_calendarioyear = [];
                        console.log($scope.agenda_calendarioyear);
                    }
                    
                    uiCalendarConfig.calendars.myCalendar1.fullCalendar('removeEvents');
                    uiCalendarConfig.calendars.myCalendar1.fullCalendar('addEventSource', $scope.agenda_calendario);
                    $scope.renderCalendar('myCalendar1');
                    $scope.stopwaiting();
                    //$scope.tableParams.reload();
                } else {
                    $scope.agenda_calendarioyear = [];
                    //$scope.tableParams.reload();
                    SweetAlert.swal({
                        title: "Error",
                        text: response.data.message,
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
                $http({
                    method: 'post',
                    url: url,
                    data: $.param({
                        'type': 'gethistorialdia_d1',
                        'fecha': $scope.formattedDate(null,5)
                    }),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }).
                then(function (response) {
                    if (response.data.success) {
                        console.log(response.data.success);
                        if (!angular.isUndefined(response.data.data)) {
                            $scope.HistorialHoy = response.data.data;
                            console.log("gethistorialdia_d1");
                            console.log($scope.HistorialHoy);
                        } else {
                            $scope.HistorialHoy = [];
                            console.log($scope.HistorialHoy);
                        }
                        $scope.stopwaiting();
                        //$scope.tableParams.reload();
                    } else {
                        $scope.HistorialHoy = [];
                        //$scope.tableParams.reload();
                        SweetAlert.swal({
                            title: "Error",
                            text: response.data.message,
                            type: "error",
                            confirmButtonColor: "#5cb85c"
                        });
                    }
                    $http({
                        method: 'post',
                        url: url,
                        data: $.param({
                            'type' : 'getaccionesdiariasxusuario_d1', 
                            'user' :  $rootScope.user.user, 
                            'fecha':  $scope.formattedDate(null,5) 
                        }),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    }).
                    then(function (response) {
                        if (response.data.success) {
                            console.log(response.data.success);
                            if (!angular.isUndefined(response.data.data)) {
                                $scope.accionesdiariasxusuario_d1 = response.data.data;
                                console.log("getaccionesdiariasxusuario_d1");
                                console.log($scope.accionesdiariasxusuario_d1);
                                for(var i=0; i<=response.data.data.length-1; i++) {
                                    switch(response.data.data[i].accion) {
                                        case 'Llamada' :
                                            $rootScope.totales.Llamadas = response.data.data[i].total;
                                            break;
                                        case 'Correo' :
                                            $rootScope.totales.Correos = response.data.data[i].total;
                                            break;
                                        case 'Cita' :
                                            $rootScope.totales.Citas = response.data.data[i].total;
                                            break;
                                        case 'Visita' :
                                            $rootScope.totales.Visitas = response.data.data[i].total;
                                            break;
                                        case 'Otra' :
                                            $rootScope.totales.Otros = response.data.data[i].total;
                                            break;
                                        case 'Contacto Efectivo' :
                                            $rootScope.totales.ContactoEfectivo = response.data.data[i].total;
                                            break;
                                    }
                                }
                            } else {
                                $scope.accionesdiariasxusuario_d1 = [];
                                console.log($scope.accionesdiariasxusuario_d1);
                            }
                            //$scope.reloadView();
                            $scope.stopwaiting();
                            //$scope.tableParams.reload();
                        } else {
                            $scope.accionesdiariasxusuario_d1 = [];
                            //$scope.tableParams.reload();
                            SweetAlert.swal({
                                title: "Error",
                                text: response.data.message,
                                type: "error",
                                confirmButtonColor: "#5cb85c"
                            });
                        }
                    }, function (error) {
                        $scope.stopwaiting();
                        $scope.accionesdiariasxusuario_d1 = [];
                        //$scope.tableParams.reload();
                        SweetAlert.swal({
                            title: "Error",
                            text: error.statusText,
                            type: "error",
                            confirmButtonColor: "#5cb85c"
                        });
                  }); 
                }, function (error) {
                    $scope.stopwaiting();
                    $scope.HistorialHoy = [];
                    //$scope.tableParams.reload();
                    SweetAlert.swal({
                        title: "Error",
                        text: error.statusText,
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                });   
            }, function (error) {
                $scope.stopwaiting();
                $scope.agenda_calendarioyear = [];
                //$scope.tableParams.reload();
                SweetAlert.swal({
                    title: "Error",
                    text: error.statusText,
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }, function (error) {
            $scope.stopwaiting();
            $scope.agenda_calendario = [];
            //$scope.tableParams.reload();
            SweetAlert.swal({
                title: "Error",
                text: error.statusText,
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    
    $scope.post = {};
    
    
    $scope.filtrocalendario = {
        tipoevento : "Ambos",
        ejecutivo : "Todos"
    };

    $scope.usuarioscalendario = []


    $scope.agenda_calendario = {
        events : []
    };
   
   

    $scope.eventRender = function( event, element, view ) {
        element.attr({'tooltip': event.tooltip,
                      'tooltip-append-to-body': true});
        if(event.imageurl) {
            element.find("div.fc-content").prepend("<img src='" + event.imageurl +"' width='30' height='30' style='border-radius: 100px'>");
        }
        $compile(element)($scope);
        return ['Ambos', event.tipoevento].indexOf($scope.filtrocalendario.tipoevento) >= 0 && ['Todos', event.ejecutivo].indexOf($scope.filtrocalendario.ejecutivo) >= 0;
    };

    $scope.uiConfig = {
      calendar:{
        editable: true,
        header:{
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay '
        },
        eventClick: $scope.alertEventOnClick,
        eventDrop: $scope.alertOnDrop,
        eventResize: $scope.alertOnResize,
        eventRender: $scope.eventRender,
        dayClick: $scope.dia,
        weekends: true, 
        titleFormat: 'MMMM YYYY',
        buttonText: {
            today:    'Hoy',
            month:    'Mes',
            week:     'Semana',
            day:      'Día',
            list:     'Lista'
        },
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        eventLimit: true,
        eventLimitText: 'más',
        views: {
            month: {
                eventLimit: 10
            }
        },
      }
    };

    

    $scope.renderCalendar = function(calendar) {
      $timeout(function() {
        if(uiCalendarConfig.calendars[calendar]){
          uiCalendarConfig.calendars[calendar].fullCalendar('render');
        }
      });
    };

    $scope.rerenderCalendar = function() {
        $timeout(function() {
            uiCalendarConfig.calendars.myCalendar1.fullCalendar('rerenderEvents');
        });
    };

  
    $scope.optionsfiltro = [];

    $scope.agregausuariofiltrocalendario = function(nombre) {
        if($scope.usuarioscalendario.indexOf(nombre) == -1) {
            $scope.usuarioscalendario.push(nombre);
        }
    }


    $scope.formattedDate = function (date, estilofecha) {
        estilofecha = typeof estilofecha !== 'undefined' ? estilofecha : 1;
        var d = new Date(date || Date.now());
        var hours = '' + d.getHours(),
            minutes = '' + d.getMinutes();

        var month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        if (hours.length < 2) hours = '0' + hours;
        if (minutes.length < 2) minutes = '0' + minutes;

        switch(estilofecha) {
            case 1:
                return [year, month, day].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 5:
                return [year, month, day].join('-');
                break;
            case 3:
                return [month, day, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 2:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 4:
                return [hours, minutes].join(':');
                break;
            default:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
        }
    };

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
        k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
        }
        return false;
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }



}]);